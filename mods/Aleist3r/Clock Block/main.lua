--[[
	Clock Block v1.0 by Aleist3r
	Upgrade code v0.1 by mintmike fullrest.ru
--]]

local common = {}
common.version = 1.0

local defaultConfig = {
	version = common.version,
	components = {
		turnOnClock = true,
		twelveHourMode = false,
		clockBottom = false,
		isGameTime = false,
	},
}
local configJson = mwse.loadConfig("Clock Block")
local config = {}

if configJson and configJson.version and common.version == configJson.version
then config = configJson
else config = defaultConfig
end

local UI, UI_ClockBlock
local lt = os.time()

local function onClockUpdate()

	if lt == os.time() then return end
	lt = os.time

	local realDate = os.date("*t")
	local realTime
	if (config.components.isGameTime == false) then
		if (config.components.twelveHourMode == true) then
			if (realDate.hour > 12) then
				local timeHour
				if (realDate.hour > 13) then
					timeHour = realDate.hour - 12
				else
					timeHour = realDate.hour
				end
				if (realDate.min < 10) then
					realTime = (timeHour .. ":0" .. realDate.min .. "pm")
				else
					realTime = (timeHour .. ":" .. realDate.min .. "pm")
				end
			else
				if (realDate.min < 10) then
					realTime = (realDate.hour .. ":0" .. realDate.min .. "am")
				else
					realTime = (realDate.hour .. ":" .. realDate.min .. "am")
				end
			end
		else
			if (realDate.min < 10) then
				realTime = (realDate.hour .. ":0" .. realDate.min)
			else
				realTime = (realDate.hour .. ":" .. realDate.min)
			end
		end
	else
		local gameTime = tes3.getGlobal("GameHour")
		local hourString

		if (config.components.twelveHourMode == true) then
			local isPM = false
			if (gameTime > 12) then
				isPM = true
				if (gameTime > 13) then
					gameTime = gameTime - 12
				end
			end

			if gameTime < 10 then 
				hourString = string.sub(gameTime, 1, 1)
			else
				hourString  = string.sub(gameTime, 1, 2)
			end

			local minuteTime = ( gameTime - hourString ) * 60
			local minuteString
			if minuteTime < 10 then
				minuteString = "0" .. string.sub( minuteTime, 1, 1 )
			else
				minuteString = string.sub ( minuteTime , 1, 2)
			end
			realTime = (hourString .. ":" .. minuteString .. (isPM and "pm" or "am"))
		else
			if gameTime < 10 then 
				hourString = string.sub(gameTime, 1, 1)
			else
				hourString  = string.sub(gameTime, 1, 2)
			end

			local minuteTime = ( gameTime - hourString ) * 60
			local minuteString
			if minuteTime < 10 then
				minuteString = "0" .. string.sub( minuteTime, 1, 1 )
			else
				minuteString = string.sub ( minuteTime , 1, 2)
			end
			realTime = (hourString .. ":" .. minuteString)
		end
	end
	if UI_ClockBlock then UI_TimeText.text = realTime	end
end

local function onMenuClock(e)
	if not e.newlyCreated then return	end

	UI = tes3ui.findMenu(tes3ui.registerID("MenuMulti"))

	local UI_MiniMap = UI:findChild(tes3ui.registerID("MenuMap_panel"))

	local UI_MiniMapBlock = UI_MiniMap.parent

	UI_MiniMapBlock.flowDirection = "top_to_bottom"
	UI_MiniMapBlock.alpha = tes3.worldController.menuAlpha

	UI_ClockBlock = UI_MiniMapBlock:createThinBorder({id = tes3ui.registerID("Aleist3r:ClockBlock")})
	UI_ClockBlock.flowDirection = "left_to_right"
	UI_ClockBlock.width= 65
	UI_ClockBlock.height = 20

	UI_TimeText = UI_ClockBlock:createLabel({ id = tes3ui.registerID("Aleist3r:ClockText") })
	UI_TimeText.absolutePosAlignX = 0.5

	if not config.components.clockBottom then
		UI_MiniMapBlock:reorderChildren(UI_MiniMap, UI_ClockBlock, 1)
	end

	UI:updateLayout()
end

local function hotResetClock()
	if config.components.turnOnClock and tes3.mobilePlayer then
		UI_ClockBlock:destroy()
		onMenuClock({newlyCreated = true})
	end
end

local function onMod()
	config.components.turnOnClock = true
	if tes3.mobilePlayer then	onMenuClock({newlyCreated = true}) end
	event.register("uiActivated", onMenuClock, { filter = "MenuMulti" })
	event.register("enterFrame", onClockUpdate)
end

local function offMod()
	config.components.turnOnClock = false
	if tes3.mobilePlayer and UI_ClockBlock then	UI_ClockBlock:destroy()	end
	event.unregister("uiActivated", onMenuClock, { filter = "MenuMulti" })
	event.unregister("enterFrame", onClockUpdate)
end

local modConfig = {}

function modConfig.onCreate(container)
	local pane = container:createThinBorder{}
	pane.widthProportional = 1.0
	pane.heightProportional = 1.0
	pane.paddingAllSides = 12
    pane.flowDirection = "top_to_bottom"
	
	local header = pane:createLabel({ text = "���� �� Aleist3r, ������ 1.1" })
  header.borderAllSides = 6
	header.color = tes3ui.getPalette("header_color")
	
	local horizontalBlock1 = pane:createBlock({})
	horizontalBlock1.flowDirection = "left_to_right"
	horizontalBlock1.widthProportional = 1.0
	horizontalBlock1.height = 32
	horizontalBlock1.borderTop = 6
	horizontalBlock1.borderLeft = 6
	horizontalBlock1.borderRight = 6

	local label1 = horizontalBlock1:createLabel({ text = "���������� ����:" })
	label1.absolutePosAlignX = 0.0
	label1.absolutePosAlignY = 0.5

	local buttonTurnOn = horizontalBlock1:createButton({ text = config.components.turnOnClock and tes3.findGMST(tes3.gmst.sOn).value or tes3.findGMST(tes3.gmst.sOff).value })
	buttonTurnOn.absolutePosAlignX = 1.0
	buttonTurnOn.absolutePosAlignY = 0.5
	buttonTurnOn.paddingTop = 3
	buttonTurnOn:register("mouseClick", function(e)
			if config.components.turnOnClock then offMod() else onMod() end
			buttonTurnOn.text = (config.components.turnOnClock and tes3.findGMST(tes3.gmst.sOn).value or tes3.findGMST(tes3.gmst.sOff).value)
	end)

	local horizontalBlock2 = pane:createBlock({})
	horizontalBlock2.flowDirection = "left_to_right"
	horizontalBlock2.widthProportional = 1.0
	horizontalBlock2.height = 32
	horizontalBlock2.borderRight = 6
	horizontalBlock2.borderLeft = 6

	local label2 = horizontalBlock2:createLabel({ text = "��� �����:" })
	label2.absolutePosAlignX = 0.0
	label2.absolutePosAlignY = 0.5

	local buttonClockMode = horizontalBlock2:createButton({ text = config.components.twelveHourMode and "12 �����" or "24 ����" })
	buttonClockMode.absolutePosAlignX = 1.0
	buttonClockMode.absolutePosAlignY = 0.5
	buttonClockMode.paddingTop = 3
	buttonClockMode:register("mouseClick", function(e)
		config.components.twelveHourMode = not config.components.twelveHourMode
		buttonClockMode.text = (config.components.twelveHourMode and "12 �����" or "24 ����")
	end)
	
	local horizontalBlock3 = pane:createBlock({})
	horizontalBlock3.flowDirection = "left_to_right"
	horizontalBlock3.widthProportional = 1.0
	horizontalBlock3.height = 32
	horizontalBlock3.borderRight = 6
	horizontalBlock3.borderLeft = 6

	local label3 = horizontalBlock3:createLabel({ text = "������� ����� ������������ ����-�����:" })
	label3.absolutePosAlignX = 0.0
	label3.absolutePosAlignY = 0.5

	local buttonClockPos = horizontalBlock3:createButton({ text = config.components.clockBottom and "�����" or "������" })
	buttonClockPos.absolutePosAlignX = 1.0
	buttonClockPos.absolutePosAlignY = 0.5
	buttonClockPos.paddingTop = 3
	buttonClockPos:register("mouseClick", function(e)
		config.components.clockBottom = not config.components.clockBottom
		buttonClockPos.text = (config.components.clockBottom and "�����" or "������")
		hotResetClock()
	end)

	local horizontalBlock4 = pane:createBlock({})
	horizontalBlock4.flowDirection = "left_to_right"
	horizontalBlock4.widthProportional = 1.0
	horizontalBlock4.height = 32
	horizontalBlock4.borderBottom = 6
	horizontalBlock4.borderLeft = 6
	horizontalBlock4.borderRight = 6
	local label4 = horizontalBlock4:createLabel({ text = "������������ �����:" })
	label4.absolutePosAlignX = 0.0
	label4.absolutePosAlignY = 0.5

	local buttonClockType = horizontalBlock4:createButton({ text = config.components.isGameTime and "����� ����" or "�������� �����" })
	buttonClockType.absolutePosAlignX = 1.0
	buttonClockType.absolutePosAlignY = 0.5
	buttonClockType.paddingTop = 3
	buttonClockType:register("mouseClick", function(e)	
		config.components.isGameTime = not config.components.isGameTime
		buttonClockType.text = (config.components.isGameTime and "����� ����" or "�������� �����")
	end)

	local credits = pane:createLabel({ text = "������:" })
	credits.color = tes3ui.getPalette("header_color")
	credits.borderLeft = 6
	credits.borderRight = 6
	credits.borderTop = 6

	local UI_coding = pane:createLabel({ text = "����������������: Aleist3r, Merlord"})
	UI_coding.borderLeft = 6
	UI_coding.borderRight = 6

	local UI_Help = pane:createLabel({ text = "��������� ������: Morrowind Modding Comunity Discord"})
	UI_Help.borderLeft = 6
	UI_Help.borderRight = 6
	UI_Help.layoutWidthFraction = 1.0
  UI_Help.wrapText = true

	pane:updateLayout()
end

function modConfig.onClose(container)
	mwse.saveConfig("Clock Block", config)
end
modConfig.config = config

local function registerModConfig()
	mwse.registerModConfig("����", modConfig)
end
event.register("modConfigReady", registerModConfig)

function init()
	if config.components.turnOnClock then onMod() end
end
event.register("initialized", init)