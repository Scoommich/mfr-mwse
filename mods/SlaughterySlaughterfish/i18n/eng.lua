return {
	["mcm.modname"] = "SlaughterySlaughterfish",
	["mcm.page.label"] = "Settings",
	["mcm.page.description"] = "Slaughterfish attack NPCs that swim near them.",
	["mcm.detectRate.label"] = "Detection Rate",
	["mcm.detectRate.slider"] = "Time in seconds between running detection script",
	["mcm.detectRange.label"] = "Detection Range",
	["mcm.detectRange.slider"] = "Maximum distance for actor detection",
}
