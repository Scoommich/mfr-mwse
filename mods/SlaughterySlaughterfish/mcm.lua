local config = require("SlaughterySlaughterfish.config")

--[[ CHECK MWSE build date & rus encoding & load translations ]]

local rus = require("SlaughterySlaughterfish.i18n.rus") -- should be encoding UTF-8 to work with i18n
local rus1251 = require("SlaughterySlaughterfish.rus1251")
if not rus1251 then rus1251 = require("SlaughterySlaughterfish.i18n.eng") end

local tr -- translate
if mwse.buildDate < 20220601 
or not rus 
or not rus["mcm.modname"] 
or #rus["mcm.modname"] == #rus1251["mcm.modname"] -- i18n.rus.lua encoding == rus1251.lua encoding
then
	mwse.log("[SlaughterySlaughterfish] Loading rus1251 translations...")
	tr = function(key) return rus1251[key] or key or "<no translate>" end
	--tr = setmetatable({}, {__call = function(self, key) return rus1251[key] or key or "<no translate>" end})
else
	tr = mwse.loadTranslations("SlaughterySlaughterfish")
end

--[[ MCM ]]

local template = mwse.mcm.createTemplate(tr("mcm.modname"))
template:saveOnClose("SlaughterySlaughterfish", config)

local page = template:createSideBarPage()
page.label = tr("mcm.page.label")
page.description = tr("mcm.page.description")
page.noScroll = false

local category = page:createCategory(tr("mcm.page.label"))

	category:createSlider({
	label = tr("mcm.detectRate.label"),
	description = tr("mcm.detectRate.slider"),
	min = 1,
	max = 30,
	step = 1,
	jump = 5,
	variable = mwse.mcm.createTableVariable{id = "detectRate", table = config },
})


	category:createSlider({
	label = tr("mcm.detectRange.label"),
	description = tr("mcm.detectRange.slider"),
	min = 1,
	max = 1024,
	step = 1,
	jump = 10,
	variable = mwse.mcm.createTableVariable{id = "detectRange", table = config },
})

mwse.mcm.register(template)

return this