--Thanks to NullCascade for this mcm menu code

local this = {}

local refreshActiveList

local blackListPane
local blackListActualPane

local function createBlackListRow(container, id)
	local row = container:createBlock({})
	row.layoutWidthFraction = 1.0
	row.autoHeight = true
	blackListActualPane = row.parent

	local label = row:createLabel({ text = id })

	local removeBtn = row:createButton({ text = "�������" })
	removeBtn.layoutOriginFractionX = 1.0
	removeBtn:register("mouseClick", function(e)
		table.removevalue(this.config.blackList, id)
		row:destroy()
		refreshActiveList()
		container:getTopLevelParent():updateLayout()
	end)

	return row
end

local function caseInsensitiveSorter(a, b)
	return string.lower(a) < string.lower(b)
end

local function refreshBlackList()
	if (blackListActualPane) then
		blackListActualPane:destroyChildren()
	end
	table.sort(this.config.blackList, caseInsensitiveSorter)
	for i = 1, #this.config.blackList do
		createBlackListRow(blackListPane, this.config.blackList[i])
	end
end

local activeListPane
local activeListActualPane

local function createActiveListRow(container, follower)
	local row = container:createBlock({})
	row.layoutWidthFraction = 1.0
	row.autoHeight = true
	activeListActualPane = row.parent

	local followerBaseId = follower.id
	if (follower.isInstance) then
		followerBaseId = follower.baseObject.id
	end

	local label = row:createLabel({ text = followerBaseId })

	if (table.find(this.config.blackList, followerBaseId) == nil) then
		local removeBtn = row:createButton({ text = "������ ������" })
		removeBtn.layoutOriginFractionX = 1.0
		removeBtn:register("mouseClick", function(e)
			table.insert(this.config.blackList, followerBaseId)
			refreshBlackList()
			removeBtn.visible = false
		end)
	end

	return row
end

refreshActiveList = function()
	if (activeListActualPane) then
		activeListActualPane:destroyChildren()
	end

	local macp = tes3.mobilePlayer
	if (macp) then
		for actor in tes3.iterate(macp.friendlyActors) do
			if (actor ~= macp) then
				createActiveListRow(activeListPane, actor.reference.object)
			end
		end
	end
end

local function createConfigSliderPackage(params)
	local horizontalBlock = params.parent:createBlock({})
	horizontalBlock.flowDirection = "left_to_right"
	horizontalBlock.layoutWidthFraction = 1.0
	horizontalBlock.height = 24

	local label = horizontalBlock:createLabel({ text = params.label })
	label.layoutOriginFractionX = 0.0
	label.layoutOriginFractionY = 0.5

	local config = params.config
	local key = params.key
	local value = config[key] or params.default or 0
	
	local sliderLabel = horizontalBlock:createLabel({ text = tostring(value) })
	sliderLabel.layoutOriginFractionX = 1.0
	sliderLabel.layoutOriginFractionY = 0.5
	sliderLabel.borderRight = 306

	local range = params.max - params.min

	local slider = horizontalBlock:createSlider({ current = value - params.min, max = range, step = params.step, jump = params.jump })
	slider.layoutOriginFractionX = 1.0
	slider.layoutOriginFractionY = 0.5
	slider.width = 300
	slider:register("PartScrollBar_changed", function(e)
		config[key] = slider:getPropertyInt("PartScrollBar_current") + params.min
		sliderLabel.text = config[key]
		if (params.onUpdate) then
			params.onUpdate(e)
		end
	end)

	return { block = horizontalBlock, label = label, sliderLabel = sliderLabel, slider = slider }
end

function this.onCreate(parent)
	blackListActualPane = nil
	activeListActualPane = nil

	local container = parent:createThinBorder({})
	container.flowDirection = "top_to_bottom"
	container.layoutHeightFraction = 1.0
	container.layoutWidthFraction = 1.0
	container.paddingAllSides = 6

	local header = container:createLabel{ text = "����� �������� �����������\n������ 0.3a �� mesafoo, ��������� 0.1 �� MintMike" }
	header.color = tes3ui.getPalette("header_color")
	header.borderBottom = 25

	local descriptionLabel
	if (tes3.mobilePlayer) then
		descriptionLabel = container:createLabel({ text = "���� �������� ����������� ��������� ����������� ������� ��������, ����, ������ ��� ����� ���������. ������ ������������� NPC, �� ���������� ������ ������������, ����� ������������. ����������� ������ ������, ����� ��������� ������ NPC �����������. ��� ��� ������� ������ ������� ������. �� ��� �� ������ ������� NPC �� ������� ������.\n" })
	else
		descriptionLabel = container:createLabel({ text = "��������� ����������� ������� ��������, ����, ������ ��� ����� ���������. ������ ������������� NPC, �� ���������� ������ ������������, ����� ������������. ��� ��������� � �������� NPC � ������ ������ ��������� ����������� ����." })
	end
	descriptionLabel.layoutWidthFraction = 1.0
	descriptionLabel.wrapText = true
	descriptionLabel.layoutHeightFraction = -1
	descriptionLabel.borderBottom = 12

	local labelBlock = container:createBlock()
	labelBlock.flowDirection = "left_to_right"
	labelBlock.widthProportional = 1.0
	labelBlock.autoHeight = true
	labelBlock:createLabel({ text = "���������� ���" })
	local labelButton = labelBlock:createButton({ text = this.config.labelName and tes3.findGMST(tes3.gmst.sYes).value or tes3.findGMST(tes3.gmst.sNo).value })
	 labelButton.absolutePosAlignX = 1.0
	 labelButton:register("mouseClick", function(e)
		this.config.labelName = not this.config.labelName
		labelButton.text = this.config.labelName and tes3.findGMST(tes3.gmst.sYes).value or tes3.findGMST(tes3.gmst.sNo).value
	end)

	local hBlock = container:createBlock()
	hBlock.flowDirection = "left_to_right"
	hBlock.widthProportional = 1.0
	hBlock.autoHeight = true
	hBlock:createLabel({ text = "���������� ����� ��������" })
	local hButton = hBlock:createButton({ text = this.config.healthBar and tes3.findGMST(tes3.gmst.sYes).value or tes3.findGMST(tes3.gmst.sNo).value })
	 hButton.absolutePosAlignX = 1.0
	 hButton:register("mouseClick", function(e)
		this.config.healthBar = not this.config.healthBar
		hButton.text = this.config.healthBar and tes3.findGMST(tes3.gmst.sYes).value or tes3.findGMST(tes3.gmst.sNo).value
	end)

	local mBlock = container:createBlock()
	mBlock.flowDirection = "left_to_right"
	mBlock.widthProportional = 1.0
	mBlock.autoHeight = true
	mBlock:createLabel({ text = "���������� ����� ����" })
	local mButton = mBlock:createButton({ text = this.config.magickaBar and tes3.findGMST(tes3.gmst.sYes).value or tes3.findGMST(tes3.gmst.sNo).value })
	 mButton.absolutePosAlignX = 1.0
	 mButton:register("mouseClick", function(e)
		this.config.magickaBar = not this.config.magickaBar
		mButton.text = this.config.magickaBar and tes3.findGMST(tes3.gmst.sYes).value or tes3.findGMST(tes3.gmst.sNo).value
	end)

	local fBlock = container:createBlock()
	fBlock.flowDirection = "left_to_right"
	fBlock.widthProportional = 1.0
	fBlock.autoHeight = true
	fBlock:createLabel({ text = "���������� ����� ������ ���" })
	local fButton = fBlock:createButton({ text = this.config.fatigueBar and tes3.findGMST(tes3.gmst.sYes).value or tes3.findGMST(tes3.gmst.sNo).value })
	 fButton.absolutePosAlignX = 1.0
	 fButton:register("mouseClick", function(e)
		this.config.fatigueBar = not this.config.fatigueBar
		fButton.text = this.config.fatigueBar and tes3.findGMST(tes3.gmst.sYes).value or tes3.findGMST(tes3.gmst.sNo).value
	end)

	local hinfBlock = container:createBlock()
	hinfBlock.flowDirection = "left_to_right"
	hinfBlock.widthProportional = 1.0
	hinfBlock.autoHeight = true
	hinfBlock:createLabel({ text = "���������� ������ ������ � ����" })
	local hinfButton = hinfBlock:createButton({ text = this.config.showFollowersOnly and tes3.findGMST(tes3.gmst.sYes).value or tes3.findGMST(tes3.gmst.sNo).value })
	 hinfButton.absolutePosAlignX = 1.0
	 hinfButton:register("mouseClick", function(e)
		this.config.showFollowersOnly = not this.config.showFollowersOnly
		hinfButton.text = this.config.showFollowersOnly and tes3.findGMST(tes3.gmst.sYes).value or tes3.findGMST(tes3.gmst.sNo).value
	end)

	createConfigSliderPackage({
		parent = container,
		label = "������� ���� �� X",
		config = this.config,
		key = "menuposX",
		min = 0,
		max = 100,
		step = 1,
		jump = 10,
	})

	createConfigSliderPackage({
		parent = container,
		label = "������� ���� �� Y",
		config = this.config,
		key = "menuposY",
		min = 0,
		max = 100,
		step = 1,
		jump = 10,
	})

	createConfigSliderPackage({
		parent = container,
		label = "������� ���������� ����, ��� (������� ��� �������� ����������)",
		config = this.config,
		key = "pollRate",
		min = 1,
		max = 5,
		step = 1,
		jump = 1,
	})

	local splitPane = container:createBlock({})
	splitPane.flowDirection = "left_to_right"
	splitPane.layoutWidthFraction = 1.0
	splitPane.layoutHeightFraction = 1.0

	do
		local blackListBox = splitPane:createBlock({})
		blackListBox.flowDirection = "top_to_bottom"
		blackListBox.layoutWidthFraction = 1.0
		blackListBox.layoutHeightFraction = 1.0
	
		local label = blackListBox:createLabel({ text = "������ ������:" })
		label.borderBottom = 6

		blackListPane = blackListBox:createVerticalScrollPane({})
		blackListPane.layoutWidthFraction = 1.0
		blackListPane.layoutHeightFraction = 1.0
		blackListPane.paddingAllSides = 6

		refreshBlackList()
	end

	if (tes3.mobilePlayer) then
		local activeListBox = splitPane:createBlock({})
		activeListBox.flowDirection = "top_to_bottom"
		activeListBox.layoutWidthFraction = 1.0
		activeListBox.layoutHeightFraction = 1.0
		activeListBox.borderLeft = 6
	
		local label = activeListBox:createLabel({ text = "������������� NPC:" })
		label.borderBottom = 6

		activeListPane = activeListBox:createVerticalScrollPane({})
		activeListPane.layoutWidthFraction = 1.0
		activeListPane.layoutHeightFraction = 1.0
		activeListPane.paddingAllSides = 6
		
		refreshActiveList()
	end
	
	container:getTopLevelParent():updateLayout()
end

-- Since we are taking control of the mod config system, we will manually handle saves. This is
-- called when the save button is clicked while configuring this mod.
function this.onClose(container)
	mwse.saveConfig("Companion Health Bars", this.config)
 this.redrawFlag = true
end

return this