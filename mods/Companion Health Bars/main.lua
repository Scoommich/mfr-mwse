--[[
	Mod Initialization: Companion Health Bars
	Author: mesafoo
	Credits: NullCascade, and all the other MWSE devs for all the work they've done on MWSE. NullCascade again for the companion detection, blacklist, and MCM menu code I've used, and the very generous MIT license his mods are under.
	
 Update: 0.1 by MintMike (fullrest.ru)
	Adds bars for your companions to the in-game HUD.

]]--

local function logMessage(messageIn, debugLevel)
	debugLevel = debugLevel or 1
	if(debugLevel > 0) then
		mwse.log(messageIn)
		if(debugLevel > 1) then
			tes3.messageBox({message = messageIn})
		end
	end
end

-- Ensure we have the features we need.
if (mwse.buildDate == nil or mwse.buildDate < 20190401) then
	logMessage("[����� �������� �����������] MWSE ���� �� ����" .. mwse.buildDate .. "������ ����� ���� �� ����� 20190401.")
	return
end

local config = mwse.loadConfig("Companion Health Bars") or 
{
 blackList = {"chargen boat guard 2"},
 pollRate = 1,
 labelName = true,
 healthBar = true,
 magickaBar = true,
 fatigueBar = true,
 showFollowersOnly = false,
 menuposX = 98,
 menuposY = 9,
}
mwse.log("[����� �������� �����������] ��������� ���������")

local modConfig = require("Companion Health Bars.mcm")
modConfig.config = config

local function registerModConfig()
	mwse.registerModConfig("����� �������� �����������", modConfig)
end
event.register("modConfigReady", registerModConfig)


local companionBarsTable = {}

local function createBars(gameHUD, parentMenu, parentMenuID, companion)
	--Create a label with the companion's name for the  bar.
	local labelMenuID = tes3ui.registerID("����� ��������:" .. companion.id .. ".�����")
 if config.labelName then
	  local labelMenu = parentMenu:createLabel{id = labelMenuID, text = companion.object.name}
	  --Add a bit of space between the label and bars. A good value here is going to depend on personal taste I suppose.
	  labelMenu.borderTop = 2
 end
 local healthBarID, healthBar
	local magickaBarID, magickaBar
	local fatigueBarID, fatigueBar

 if config.healthBar then
	 healthBarID = tes3ui.registerID("����� ��������:" .. companion.id ..".�����")
	 healthBar = parentMenu:createFillBar{id = healthBarID, current = companion.mobile.health.current, max = companion.mobile.health.base}
	healthBar.widget.fillColor = {0.8, 0.3, 0.2}
 end

 if config.magickaBar then
	 magickaBarID = tes3ui.registerID("����� ����:" .. companion.id ..".�����")
	 magickaBar = parentMenu:createFillBar{id = magickaBarID, current = companion.mobile.magicka.current, max = companion.mobile.magicka.base}
	 magickaBar.widget.fillColor = {0.2, 0.2, 0.6}
 end

 if config.fatigueBar then
	 fatigueBarID = tes3ui.registerID("����� ���:" .. companion.id ..".�����")
	 fatigueBar = parentMenu:createFillBar{id = fatigueBarID, current = companion.mobile.fatigue.current, max = companion.mobile.fatigue.base}
	 fatigueBar.widget.fillColor = {0.3, 0.6, 0.1}
 end

	--Add all our bars info to the table.
	companionBarsTable[#companionBarsTable+1] = 
{
 companionReference = companion,
 parentMenuID = parentMenuID,
 parentMenu = parentMenu,
 labelMenuID = labelMenuID,
 labelMenu = labelMenu,
 healthBarID = healthBarID,
 healthBar = healthBar,
 magickaBarID = magickaBarID,
 magickaBar = magickaBar,
 fatigueBarID = fatigueBarID,
 fatigueBar = fatigueBar,
}
end

local function inBlackList(actor)
	-- Get the ID. If we're looking at an instance, check against the base object instead.
	local id = actor.reference.id
	if actor.reference.object.isInstance then
		id = actor.reference.object.baseObject.id
	end
	--Added check to make sure there is a blacklist before using table.find on it, else we'll get errors in the MWSE log.
	return config.blackList and table.find(config.blackList, id) ~= nil
end

local companionTable = {}

local function clearTable(tableToClear)
	for i = #tableToClear, 1, -1 do
		tableToClear[i] = nil
	end
end

local function redrawBars()
	--Get the games main hud menu element.
	local gameHUDID = tes3ui.registerID("MenuMulti")
 local gameHUD = tes3ui.findMenu(gameHUDID)
 
	if #companionBarsTable ~= 0 then		
   -- Destroy the old bars parent menu, which should also destroy any child bars there may be remaining
			local parentMenu = gameHUD:findChild(companionBarsTable[#companionBarsTable].parentMenuID)
			if parentMenu then
					parentMenu:destroy()
					gameHUD:updateLayout()
					clearTable(companionBarsTable)
			end
	end
 if #companionTable == 0 then return end

	local parentMenuID = tes3ui.registerID("����� ��������: ����")
	local parentMenu = gameHUD:findChild(parentMenuID)
	if not parentMenu then
			--Create a parent menu it and fill out any needed properties.
			parentMenu = gameHUD:createRect{id = parentMenuID, {1.0, 1.0, 1.0}}
			parentMenu.autoHeight = true
			parentMenu.autoWidth = true
			parentMenu.absolutePosAlignX = ((gameHUD.width * config.menuposX / 100) / gameHUD.width)
			parentMenu.absolutePosAlignY = (1.0 - ((gameHUD.height * config.menuposY / 100) / gameHUD.height))
			parentMenu.flowDirection = "top_to_bottom"
			parentMenu.alpha = 0.4
			parentMenu.paddingAllSides = 4
 end
	--Create new bars for everyone in companionTable
	for i = #companionTable, 1, -1 do
			createBars(gameHUD, parentMenu, parentMenuID, companionTable[i])
	end
end

local function refreshBars()

 local ccount = #companionTable
 clearTable(companionTable)

	for mobileActor in tes3.iterate(tes3.mobilePlayer.friendlyActors) do
	  local animState = mobileActor.actionData.animationAttackState
	  if mobileActor ~= tes3.mobilePlayer and
     (not config.showFollowersOnly or tes3.getCurrentAIPackageId(mobileActor) == tes3.aiPackage.follow) and
     mobileActor.health.current > 0 and
     animState ~= tes3.animationState.dead and  
     animState ~= tes3.animationState.dying and
     not inBlackList(mobileActor) then
     companionTable[#companionTable+1] = mobileActor.reference
	  end
	end

 if #companionTable ~= ccount then modConfig.redrawFlag = true end
 -- redrawFlag == true if companionTable was changed or MCM options was changed
 if modConfig.redrawFlag then
   redrawBars()
   modConfig.redrawFlag = false
 end
 --if cnvalue == 0 then return end -- return, if no companions
	for i, b in ipairs(companionBarsTable) do
  if b.healthBar then
		 b.healthBar.widget.max = b.companionReference.mobile.health.base
		 b.healthBar.widget.current = b.companionReference.mobile.health.current
  end
  if b.magickaBar then
		 b.magickaBar.widget.max = b.companionReference.mobile.magicka.base
		 b.magickaBar.widget.current = b.companionReference.mobile.magicka.current
  end
  if b.fatigueBar then
		 b.fatigueBar.widget.max = b.companionReference.mobile.fatigue.base
		 b.fatigueBar.widget.current = b.companionReference.mobile.fatigue.current
  end
		if(i == #companionBarsTable) then
			b.parentMenu:updateLayout()
		end
	end
end

event.register("load", (function(e) clearTable(companionTable); clearTable(companionBarsTable) end))
event.register("loaded", (function(e) timer.start(config.pollRate, refreshBars, 0) end))

