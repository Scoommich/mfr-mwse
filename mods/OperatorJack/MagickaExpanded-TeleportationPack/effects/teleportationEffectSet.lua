local framework = include("OperatorJack.MagickaExpanded.magickaExpanded")

-- Capitals
tes3.claimSpellEffectId("TeleportToAldRuhn", 241)
tes3.claimSpellEffectId("TeleportToBalmora", 242)
tes3.claimSpellEffectId("TeleportToEbonheart", 243)
tes3.claimSpellEffectId("TeleportToVivec", 244)

-- Towns
tes3.claimSpellEffectId("TeleportToCaldera", 245)
tes3.claimSpellEffectId("TeleportToGnisis", 246)
tes3.claimSpellEffectId("TeleportToMaarGan", 247)
tes3.claimSpellEffectId("TeleportToMolagMar", 248)
tes3.claimSpellEffectId("TeleportToPelagiad", 249)
tes3.claimSpellEffectId("TeleportToSuran", 250)
tes3.claimSpellEffectId("TeleportToTelMora", 251)

-- Other
tes3.claimSpellEffectId("TeleportToMournhold", 310)


local function getDescription(location)
    return "���� ������ ������������� ��� � ".. location .."."
end
local function addTeleportationEffects()
	framework.effects.mysticism.createBasicTeleportationEffect({
		id = tes3.effect.TeleportToMournhold,
		name = "�������� � ��������",
		description = getDescription("��������"),
		baseCost = 150,
		positionCell = {
			position = { -4, 3170, 199},
			orientation = { x=0, y=0, z=0},
			cell = "Mournhold, Plaza Brindisi Dorom"
		}
	})
	framework.effects.mysticism.createBasicTeleportationEffect({
		id = tes3.effect.TeleportToTelMora,
		name = "�������� � ���� ����",
		description = getDescription("���� ����"),
		baseCost = 150,
		positionCell = {
			position = { 106925, 117169, 264},
			orientation = { x=0, y=0, z=34},
			cell = "Tel Mora"
		}
	})
	framework.effects.mysticism.createBasicTeleportationEffect({
		id = tes3.effect.TeleportToSuran,
		name = "�������� � �����",
		description = getDescription("�����"),
		baseCost = 150,
		positionCell = {
			position = { 56217, -50650, 52},
			orientation = { x=0, y=0, z=178},
			cell = "Suran"
		}
	})
	framework.effects.mysticism.createBasicTeleportationEffect({
		id = tes3.effect.TeleportToPelagiad,
		name = "�������� � ��������",
		description = getDescription("��������"),
		baseCost = 150,
		positionCell = {
			position = { 1008, -56746, 1360},
			orientation = { x=0, y=0, z=86},
			cell = "Pelagiad"
		}
	})
	framework.effects.mysticism.createBasicTeleportationEffect({
		id = tes3.effect.TeleportToMolagMar,
		name = "�������� � ����� ���",
		description = getDescription("����� ���"),
		baseCost = 150,
		positionCell = {
			position = { 106763, -61839, 780},
			orientation = { x=0, y=0, z=92},
			cell = "Molag Mar"
		}
	})
	framework.effects.mysticism.createBasicTeleportationEffect({
		id = tes3.effect.TeleportToMaarGan,
		name = "�������� � ���� ���",
		description = getDescription("���� ���"),
		baseCost = 150,
		positionCell = {
			position = { -22118, 102242, 1979},
			orientation = { x=0, y=0, z=34},
			cell = "Maar Gan"
		}
	})
	framework.effects.mysticism.createBasicTeleportationEffect({
		id = tes3.effect.TeleportToGnisis,
		name = "�������� � ������",
		description = getDescription("������"),
		baseCost = 150,
		positionCell = {
			position = { -86430, 91415, 1035},
			orientation = { x=0, y=0, z=34},
			cell = "Gnisis"
		}
	})
	framework.effects.mysticism.createBasicTeleportationEffect({
		id = tes3.effect.TeleportToCaldera,
		name = "�������� � ��������",
		description = getDescription("��������"),
		baseCost = 150,
		positionCell = {
			position = { -10373, 17241, 1284},
			orientation = { x=0, y=0, z=4},
			cell = "Caldera"
		}
	})
	framework.effects.mysticism.createBasicTeleportationEffect({
		id = tes3.effect.TeleportToVivec,
		name = "�������� � �����",
		description = getDescription("�����"),
		baseCost = 150,
		positionCell = {
			position = { 29906, -76553, 790},
			orientation = { x=0, y=0, z=178},
			cell = "Vivec"
		}
	})
	framework.effects.mysticism.createBasicTeleportationEffect({
		id = tes3.effect.TeleportToEbonheart,
		name = "�������� � ��������",
		description = getDescription("��������"),
		baseCost = 150,
		positionCell = {
			position = { 18122, -101919, 337},
			orientation = { x=0, y=0, z=268},
			cell = "Ebonheart"
		}
	})
	framework.effects.mysticism.createBasicTeleportationEffect({
		id = tes3.effect.TeleportToBalmora,
		name = "�������� � �������",
		description = getDescription("�������"),
		baseCost = 150,
		positionCell = {
			position = { -22707, -17639, 403},
			orientation = { x=0, y=0, z=0},
			cell = "Balmora"
		}
	})
	framework.effects.mysticism.createBasicTeleportationEffect({
		id = tes3.effect.TeleportToAldRuhn,
		name = "�������� � ����-���",
		description = getDescription("����-���"),
		baseCost = 150,
		positionCell = {
			position = { -16328, 52678, 1841},
			orientation = { x=0, y=0, z=92},
			cell = "Ald-Ruhn"
		}
	})
end

event.register("magicEffectsResolved", addTeleportationEffects)