local framework = include("OperatorJack.MagickaExpanded.magickaExpanded")

require("OperatorJack.MagickaExpanded-TeleportationPack.effects.teleportationEffectSet")
require("OperatorJack.MagickaExpanded-TeleportationPack.effects.blinkEffect")

local spellIds = {
  blink = "OJ_ME_BlinkSpell",


  aldruhn = "OJ_ME_TeleportToAldRuhn",
  balmora = "OJ_ME_TeleportToBalmora",
  ebonheart = "OJ_ME_TeleportToEbonheart",
  vivec = "OJ_ME_TeleportToVivec",
  
  caldera = "OJ_ME_TeleportToCaldera",
  gnisis = "OJ_ME_TeleportToGnisis",
  maargan = "OJ_ME_TeleportToMaarGan",
  molagmar = "OJ_ME_TeleportToMolagMar",
  pelagiad = "OJ_ME_TeleportToPelagiad",
  suran = "OJ_ME_TeleportToSuran",
  telmora = "OJ_ME_TeleportToTelMora",

  mournhold = "OJ_ME_TeleportToMournhold"
}

local tomes = {
  {
    id = "bk_Tome_Blink",
    spellId = spellIds.blink
  },
  {
    id = "bk_Tome_TeleAldRuhn",
    spellId = spellIds.aldruhn
  },
  {
    id = "bk_Tome_TeleBalmora",
    spellId = spellIds.balmora
  },
  {
    id = "bk_Tome_TeleEbonheart",
    spellId = spellIds.ebonheart
  },
  {
    id = "bk_Tome_TeleVivec",
    spellId = spellIds.vivec
  },
  {
    id = "bk_Tome_TeleCaldera",
    spellId = spellIds.caldera
  },
  {
    id = "bk_Tome_TeleGnisis",
    spellId = spellIds.gnisis
  },
  {
    id = "bk_Tome_TeleMaarGan",
    spellId = spellIds.maargan
  },
  {
    id = "bk_Tome_TeleMolagMar",
    spellId = spellIds.molagmar
  },
  {
    id = "bk_Tome_TelePelagiad",
    spellId = spellIds.pelagiad
  },
  {
    id = "bk_Tome_TeleSuran",
    spellId = spellIds.suran
  },
  {
    id = "bk_Tome_TeleTelMora",
    spellId = spellIds.telmora
  },

  {
    id = "bk_Tome_TeleMournhold",
    spellId = spellIds.mournhold
  },
}

local function registerSpells()
  framework.spells.createBasicSpell({
    id = spellIds.blink,
    name = "��������",
    effect = tes3.effect.blink,
    range = tes3.effectRange.target
  })

  framework.spells.createBasicSpell({
    id = spellIds.mournhold,
    name = "�������� � ��������",
    effect = tes3.effect.TeleportToMournhold,
    range = tes3.effectRange.self
  })

  framework.spells.createBasicSpell({
    id = spellIds.aldruhn,
    name = "�������� � ����-���",
    effect = tes3.effect.TeleportToAldRuhn,
    range = tes3.effectRange.self
  })
  framework.spells.createBasicSpell({
    id = spellIds.balmora,
    name = "�������� � �������",
    effect = tes3.effect.TeleportToBalmora,
    range = tes3.effectRange.self
  })
  framework.spells.createBasicSpell({
    id = spellIds.ebonheart,
    name = "�������� � ��������",
    effect = tes3.effect.TeleportToEbonheart,
    range = tes3.effectRange.self
  })
  framework.spells.createBasicSpell({
    id = spellIds.vivec,
    name = "�������� � �����",
    effect = tes3.effect.TeleportToVivec,
    range = tes3.effectRange.self
  })
  framework.spells.createBasicSpell({
    id = spellIds.caldera,
    name = "�������� � ��������",
    effect = tes3.effect.TeleportToCaldera,
    range = tes3.effectRange.self
  })
  framework.spells.createBasicSpell({
    id = spellIds.gnisis,
    name = "�������� � ������",
    effect = tes3.effect.TeleportToGnisis,
    range = tes3.effectRange.self
  })
  framework.spells.createBasicSpell({
    id = spellIds.maargan,
    name = "�������� � ����-���",
    effect = tes3.effect.TeleportToMaarGan,
    range = tes3.effectRange.self
  })
  framework.spells.createBasicSpell({
    id = spellIds.molagmar,
    name = "�������� � �����-���",
    effect = tes3.effect.TeleportToMolagMar,
    range = tes3.effectRange.self
  })
  framework.spells.createBasicSpell({
    id = spellIds.pelagiad,
    name = "�������� � ��������",
    effect = tes3.effect.TeleportToPelagiad,
    range = tes3.effectRange.self
  })
  framework.spells.createBasicSpell({
    id = spellIds.suran,
    name = "�������� � �����",
    effect = tes3.effect.TeleportToSuran,
    range = tes3.effectRange.self
  })
  framework.spells.createBasicSpell({
    id = spellIds.telmora,
    name = "�������� � ���� ����",
    effect = tes3.effect.TeleportToTelMora,
    range = tes3.effectRange.self
  })
  
  framework.tomes.registerTomes(tomes)
end

event.register("MagickaExpanded:Register", registerSpells)