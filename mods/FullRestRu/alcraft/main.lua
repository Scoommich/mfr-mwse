local skillModule = require("OtherSkills.skillModule")
local common = require("OtherSkills.common")

local function onSkillsReady()

	skillModule.registerSkill(
		"craft_weapon", 
		{	name 			=		"������������ ������", 
			icon 			=		"Icons/MFR/hud/craft_weapon.tga", 
			value			= 		5,
			progress		=		0,
			lvlcap			=		100,
			attribute 		=		tes3.attribute.strength,
			description 	= 		"����� ������������ ������ �������� ��� ��������� ����� ������ � �������, ��� ����� ������������ ���������� ����������. ��� ���� ��� ����� - ��� � ������� ����������� ���������� �� ������ ��������.",
			specialization 	= 		tes3.specialization.stealth,
			active			= 		"active"
		}
	)
	skillModule.registerSkill(
		"craft_armor", 
		{	name 			=		"������������ �����", 
			icon 			=		"Icons/MFR/hud/craft_armor.tga", 
			value			= 		5,
			progress		=		0,
			lvlcap			=		100,
			attribute 		=		tes3.attribute.endurance,
			description 	= 		"����� ������������ ����� �������� ��� ��������� ����� ����� � �������, ��� ����� ������������ ���������� �������. ��� ���� ��� ����� - ��� � ������� ����������� ���������� �� ������ ��������.",
			specialization 	= 		tes3.specialization.stealth,
			active			= 		"active"
		}
	)
	print("[alcraft INFO] ������ ����������������")
end

local skills = nil

-- ������� �������� �������� �����-������ � ������������ ��������� ����.����������
-- ��� gmst-�������� (���������� ������ �� �����)
-- ������ ������������ ��.���������� "trackercount", "trackerpause"
--( ������ ������ ����� ��� �������� �����������������)
local function updSkills()
    if not skills then return end

    -- ���� ���������� �� �����, ��������:
    -- ... = tes3.getGlobal("al_craft_weapon") -- ����.����������
    -- ... = tes3.gmst.al_craft_weapon         -- ����.���������

    -- ��������� �������� �����
    skills["craft_weapon"].value = tes3.getGlobal("al_WeaponCraft") -- << ���������� ������
    skills["craft_armor"].value  = tes3.getGlobal("al_ArmorCraft") -- << ���������� ������
    skills["craft_weapon"].progress = tes3.getGlobal("al_craft_weapon") -- << ���������� ������
    skills["craft_armor"].progress  = tes3.getGlobal("al_craft_armorer") -- << ���������� ������
    common.updateSkillList()

--[[  -- �������������� ������� �� ���������� �������� ����� � ���������

    skillModule.incrementSkill("craft_weapon", {
      value    = tes3.getGlobal("trackercount"), -- << ���������� ������
      progress = tes3.getGlobal("trackercount") -- << ���������� ������
    })
    skillModule.incrementSkill("craft_armor", {
      value    = tes3.getGlobal("trackerpause"), -- << ���������� ������
      progress = tes3.getGlobal("trackerpause") -- << ���������� ������
    })
]]--
end

local function onLoaded()
    if not tes3.getPlayerRef().data or not tes3.getPlayerRef().data.otherSkills then return end
    skills = tes3.getPlayerRef().data.otherSkills
end
event.register("loaded", onLoaded)

local function init()
    -- �������� ����� ��� �������� ������ [MFR]MWSE � [MFR]OpenMW
    if tes3.isModActive("Morrowind.esm") then
        tes3.setGlobal ("al_mwse_check", 1)
        event.register("OtherSkills:Ready", onSkillsReady)
        event.register("keyDown", updSkills)
    end
end
event.register("initialized", init)


--[[ ���� ���� ������� ��� ������������ ����������������� �������, �� ����� �������

--  ��������� ��.���������� "trackercount" �� 1
local function incWeaponSkill()
  tes3.setGlobal("trackercount", tes3.getGlobal("trackercount") + 1)
  tes3.messageBox("tes3.gmst.al_craft_weapon = " .. tostring(tes3.getGlobal("trackercount")))
end

--  ��������� ��.���������� "trackerpause" �� 1
local function incArmorSkill()
  tes3.setGlobal("trackerpause", tes3.getGlobal("trackerpause") + 1)
  tes3.messageBox("tes3.gmst.al_craft_armor = " .. tostring(tes3.getGlobal("trackerpause")))
end

event.register("keyDown", incWeaponSkill, { filter = tes3.scanCode.o }) -- ��� ������� o
event.register("keyDown", incArmorSkill, { filter = tes3.scanCode.p })  -- ��� ������� p

]]--

