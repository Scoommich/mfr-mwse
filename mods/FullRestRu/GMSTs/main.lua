--[[
  ��������� Morrownid
  
  ������ ��� ����������� �������� ��������� GMST (game settings)
]]

local modConfig = {}
modConfig.config = mwse.loadConfig("GMSTs config") or json.loadfile("mods/FullRestRu/GMSTs/default GMSTs config")
json.savefile("test", modConfig.config)

-- ����� gmst �� ����������� � ������� ��������
local function setGMSTs()
  for id, t in ipairs(modConfig.config) do
    tes3.findGMST(id).value = t.value
  end
end
event.register("initialized", setGMSTs)

local function createConfigSliderPackage(params)

	local horizontalBlock = params.parent:createBlock({})
	horizontalBlock.flowDirection = "left_to_right"
	horizontalBlock.layoutWidthFraction = 1.0
	horizontalBlock.height = 24

	local config = params.config
	local key = params.key
	local value = config[key].value or params.default or 0
	local mult = params.mult or 1
	
	local label = horizontalBlock:createLabel
		{ text = string.format(params.label, value) }
	label.layoutOriginFractionX = 0.0
	label.layoutOriginFractionY = 0.5
	
	local sliderLabel = horizontalBlock:createLabel({ text = tostring(value) })
	sliderLabel.layoutOriginFractionX = 1.0
	sliderLabel.layoutOriginFractionY = 0.5
	sliderLabel.borderRight = 306

	local range = params.max - params.min

	local slider = horizontalBlock:createSlider
	{
	 current = value/mult - params.min,
	 max = range,
	 step = params.step,
	 jump = params.jump,
	}
	slider.layoutOriginFractionX = 1.0
	slider.layoutOriginFractionY = 0.5
	slider.width = 300
	slider:register("PartScrollBar_changed", function(e)
		   config[key].value = (slider:getPropertyInt("PartScrollBar_current") + params.min)*mult
		   sliderLabel.text = tostring(config[key].value)
		   label.text = string.format(params.label, config[key].value)
		   if params.onUpdate then
			    params.onUpdate(e)
		   end
	  end)

	if (params.tooltip) then
		local tooltipType = type(params.tooltip)
		if (tooltipType == "string") then
			slider:register("help", function(e)
				local tooltipMenu = tes3ui.createTooltipMenu()
				local tooltipText = tooltipMenu:createLabel({ text = params.tooltip })
				tooltipText.wrapText = true
			end)
		elseif (tooltipType == "function") then
			slider:register("help", params.tooltip)
		end
	end

	return { block = horizontalBlock, label = label, sliderLabel = sliderLabel, slider = slider }
end

function modConfig.onCreate(container)

	local pane = container:createThinBorder{}
	pane.widthProportional = 1.0
	pane.heightProportional = 1.0
	pane.paddingAllSides = 12
	pane.flowDirection = "top_to_bottom"

	local header = pane:createLabel{ text = "��������� Morrownid" }
	header.color = tes3ui.getPalette("header_color")
	header.borderBottom = 25
	
 local descr = pane:createLabel{}
 descr.wrapText = true
 descr.height = 1
 descr.layoutWidthFraction = 1.0
 descr.layoutHeightFraction = -1.0
 descr.borderBottom = 25
 descr.text = "����� ����� ��������������� ��������� GMST. ���������� �� ���������."
	
	for id, t in pairs(modConfig.config) do
	  createConfigSliderPackage
	  {
		  parent = pane,
		  label = t.label or id,
		  config = modConfig.config,
		  key = id,
		  default = t.default or 1,
		  mult = t.mult or 1,
		  min = t.min or 0,
		  max = t.max or 1,
	  	step = t.step or 1,
		  jump = t.jump or 1,
		  tooltip = t.tooltip or id..", default = "..tostring(t.default),
	  	onUpdate = function (e)
      tes3.findGMST(id).value = t.value
	   end
	  }
 end
end

function modConfig.onClose(container)
	mwse.saveConfig("GMSTs config", modConfig.config, { indent = true })
end

local function registerModConfig()
	mwse.registerModConfig("= ��������� Morrowind =", modConfig)
end
event.register("modConfigReady", registerModConfig)
