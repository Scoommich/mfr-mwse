--[[
  The Flames Hurts v0.2 by MintMike, Fullrest.ru

  Now the fire in Morrowind is dangerous! Mod is compatible with Tamriel Rebuilt.
]]

local act = {}  -- active mobiles (NPCs and creatures)
local dead = {} -- dead mobiles (NPCs and creatures)

local function onLoad()
  act = {}
  dead = {}
end

local function add(e)

  if not e.reference.mobile then
    --mwse.log("[TheFlamesHurts] BAD ref mobile for "..e.reference.id) 
    return
  end
  local r = e.reference
  if r.disabled or r.deleted or not r.mobile.health then return end

	 if r.mobile.health.current <= 0 and 
     r.mobile.actionData.animationAttackState == tes3.animationState.dead then
    dead[r.id] = r
  else
    act[r.id] = r
  end
end

local function del(e)
  act[e.reference.id] = nil
  dead[e.reference.id] = nil
end

local flames = {}

local function onCellChanged(e)

 flames = {}
 local n = 1
	for ref in tes3.getPlayerCell():iterateReferences() do
   -- search for all fire objects in PlayerCell
   if ref.object.objectType == tes3.objectType.light and ref.object.isFire then
     local fid = ref.id:lower()

     if (ref.object.sound and ref.object.sound.id:lower():find("fire")) or
        fid:find("fire") or fid:find("braz") or fid:find("logpile") or fid:find("bm_tomb") then 
       if not fid:find("torch") and not fid:find("lamp") and not fid:find("sconce") then
         flames[n] = ref
         n = n + 1
       end
     end
   end
 end
end

local time_sim = os.time()

local function onSim(e)

  if (os.time() < time_sim + 1) then return end
  time_sim = os.time()

  if #flames == 0 then return end
  local player = tes3.mobilePlayer

  -- for mobilePlayer
  local v = 1 - player.resistFire/100 -- vulnerability to fire
  if v > 0.1 then
   for n, f in pairs(flames) do
    if not f.disabled then
     local rad = f.object.radius
             if rad > 400 then rad = 400 end
     if f and math.abs(player.position.x - f.position.x) < rad*0.25 and 
             math.abs(player.position.y - f.position.y) < rad*0.25 and
             math.abs(player.position.z - f.position.z - rad*0.25) < rad*0.5 then

      local dx = player.position.x - f.position.x
      local dy = player.position.y - f.position.y
      local dist = dx*dx + dy*dy
      local damage
      if dist > rad*rad*0.03 then damage = 2*v
      elseif dist > rad*rad*0.015 then damage = 8*v
      else damage = 16*v
      end
      -- apply health damage
      player.health.current = player.health.current - damage
      player:applyHealthDamage(true)
      if damage >= player.health.current/20 then
        tes3.playVoiceover
        {
         actor = player,
         voiceover = 4 -- hit
        }
      end
     end
    end
   end
  end

  -- for active mobiles (NPCs and creatures)
  for nid, npc in pairs(act) do
   local m = npc.mobile
   if m.health.current <= 0 and
     m.actionData.animationAttackState == tes3.animationState.dead then
     act[npc.id] = nil
     dead[npc.id] = npc
   else
     local v = 1 - m.resistFire/100 -- vulnerability to fire
     if m.cell == tes3.getPlayerCell() and v > 0.1 then
      for n, f in pairs(flames) do
       if not f.disabled then
        local rad = f.object.radius
        if rad > 400 then rad = 400 end
        if f and math.abs(m.position.x - f.position.x) < rad*0.25 and 
                math.abs(m.position.y - f.position.y) < rad*0.25 and
                math.abs(m.position.z - f.position.z - rad*0.25) < rad*0.5 then

         local dx = m.position.x - f.position.x
         local dy = m.position.y - f.position.y
         local dist = dx*dx + dy*dy
         local damage
         if dist > rad*rad*0.03 then damage = 2*v
         elseif dist > rad*rad*0.015 then damage = 8*v
         else damage = 16*v
         end
         -- apply health damage
         m.health.current = m.health.current - damage
         m:applyHealthDamage(true)
         if damage >= m.health.current/20 then
           tes3.playVoiceover
           {
            actor = m,
            voiceover = 4 -- hit
           }
         end
        end
       end
      end
     end
   end
  end
end

local function onInit()
  event.register("load", onLoad)
  event.register("mobileActivated", add)
  event.register("mobileDeactivated", del)
  event.register("simulate", onSim)
  event.register("cellChanged", onCellChanged)
end
event.register("initialized", onInit)
