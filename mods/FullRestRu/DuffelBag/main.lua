
local modConfig = require("FullRestRu.DuffelBag.mcm")

local function onContainerClosed(e)

  if modConfig.config.takeBagAway and string.find(e.reference.id, "sack_noCollision") and
  #e.reference.object.inventory == 0 then
    tes3.deleteObject(e.reference.object)
  end
end

local function transferItems()

  if tes3.menuMode() then return end
  
  if #tes3.player.object.equipment + 1 >= #tes3.player.object.inventory then
    tes3.messageBox("���� ����� �����")
    return
  end
  
  local pos = tes3vector3:new(
  tes3.mobilePlayer.position.x + 50,
  tes3.mobilePlayer.position.y + 50,
  tes3.mobilePlayer.position.z + 10
  )
  
  local comsack = tes3.createReference
  {
   object = "sack_noCollision",
   position = pos,
   cell = tes3.getPlayerCell()
  } 
  comsack.object.capacity = modConfig.config.capacity
   
   local equip = {}
   for i, node in pairs(tes3.player.object.equipment) do 
     equip[node.object.id] = node
   end

   for i, stack in pairs(tes3.player.object.inventory) do
     if (stack.object.weight * stack.count) >= modConfig.config.minWeight and
      not equip[stack.object.id] then
       tes3.transferItem
       {
        from = tes3.player,
        to = comsack,
        item = stack.object,
        count = stack.count,
        playSound = true
       }
     end
   end
   tes3.messageBox("���� � ��������")
end

function modConfig.onMod()
  event.register("containerClosed", onContainerClosed)
  event.register("keyUp", transferItems, { filter = tes3.scanCode.backspace })
end
if modConfig.config.modEnable then modConfig.onMod() end

function modConfig.offMod()
  event.unregister("containerClosed", onContainerClosed)
  event.unregister("keyUp", transferItems, { filter = tes3.scanCode.backspace })
end

local function registerModConfig()
	 mwse.registerModConfig("��������", modConfig)
end
event.register("modConfigReady", registerModConfig)
