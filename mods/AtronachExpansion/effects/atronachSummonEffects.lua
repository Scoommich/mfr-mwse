local framework = include("OperatorJack.MagickaExpanded.magickaExpanded")

tes3.claimSpellEffectId("summonAshGolem", 7700)
tes3.claimSpellEffectId("summonBoneGolem", 7701)
tes3.claimSpellEffectId("summonCrystalGolem", 7702)
tes3.claimSpellEffectId("summonFleshAtronach", 7703)
tes3.claimSpellEffectId("summonIronGolem", 7704)
tes3.claimSpellEffectId("summonSwampMyconid", 7705)
tes3.claimSpellEffectId("summonTelvanniMyconid", 7706)



local function getDescription(creatureName)
    return "����� ".. creatureName .." �� ���������."..
    " �� ���������� � ����� ����� ����� ������������ � ������� ����� ��������, ������� ������� �����������, ����"..
    " ������ �� ���������� ��� ���������� �������� �� ����� �����. ����� ������, ��� ����� �������� �������������,"..
    " ���������� �������� ��������, ����������� � ��������. �� ������ � ������ ��������� ������� �� ��� � ���������� ��������."
end
local function addSummoningEffects()
	framework.effects.conjuration.createBasicSummoningEffect({
		id = tes3.effect.summonAshGolem,
		name = "����� ���������� ������",
		description = getDescription("Ash Golem"),
		baseCost = 18,
		creatureId = "ash_golem_summon",
		icon = "EVA1\\k\tx_s_smmn_ashglm.dds"
	})
	framework.effects.conjuration.createBasicSummoningEffect({
		id = tes3.effect.summonBoneGolem,
		name = "����� ��������� ������",
		description = getDescription("Bone Golem"),
		baseCost = 16,
		creatureId = "Bone_Golem_summon",
		icon = "EVA1\\k\\tx_s_smmn_bneglm.dds"
	})
	framework.effects.conjuration.createBasicSummoningEffect({
		id = tes3.effect.summonCrystalGolem,
		name = "����� ���������������� ������",
		description = getDescription("Crystal Golem"),
		baseCost = 42,
		creatureId = "atronach_crystal_summon",
		icon = "EVA1\\k\\tx_s_smmn_crstlglm.dds"
	})
	framework.effects.conjuration.createBasicSummoningEffect({
		id = tes3.effect.summonFleshAtronach,
		name = "����� �������� �����",
		description = getDescription("Flesh Atronach"),
		baseCost = 36,
		creatureId = "atronach_flesh_summon",
		icon = "EVA1\\k\\tx_s_smmn_flhatrnh.dds"
	})
	framework.effects.conjuration.createBasicSummoningEffect({
		id = tes3.effect.summonIronGolem,
		name = "����� ��������� ������",
		description = getDescription("Iron Golem"),
		baseCost = 24,
		creatureId = "atronach_iron_summon",
		icon = "EVA1\\k\\tx_s_smmn_irnatrnh.dds"
	})
	framework.effects.conjuration.createBasicSummoningEffect({
		id = tes3.effect.summonSwampMyconid,
		name = "����� ��������� ��������",
		description = getDescription("Swamp Myconid"),
		baseCost = 12,
		creatureId = "atronach_swamp_summon",
		icon = "EVA1\\k\\tx_s_smmn_swpmycnd.dds"
	})
	framework.effects.conjuration.createBasicSummoningEffect({
		id = tes3.effect.summonTelvanniMyconid,
		name = "����� �������� ��������",
		description = getDescription("Telvanni Myconid"),
		baseCost = 50,
		creatureId = "atronach_telvanni_summ",
		icon = "EVA1\\k\\tx_s_smmn_telmycnd.dds"
	})
end

event.register("magicEffectsResolved", addSummoningEffects)