local framework = include("OperatorJack.MagickaExpanded.magickaExpanded")
if (framework == nil) then
    local function warning()
        tes3.messageBox(
            "[Atronach Expansion ERROR] ������ Magicka Expanded �� ����������!"
            .. " ��� ����� ����� ���������� ���, ����� ������������ ���� ���."
        )
    end
    event.register("initialized", warning)
    event.register("loaded", warning)
    return
end

require("AtronachExpansion.effects.atronachSummonEffects")

local spellIds = {
  ashGolem = "summon_ash_golem",
  boneGolem = "summon_bone_golem",
  crystalGolem = "summon_crystal_golem",
  fleshAtronach = "summon_flesh_atronach",
  ironGolem = "summon_iron_atronach",
  swampMyconid = "summon_swamp_atronach",
  telvanniMyconid = "summon_telvanni_atronach",
}

local tomes = {
  {
    id = "bk_Tome_SummAshGolem",
    spellId = spellIds.ashGolem
  },
  {
    id = "bk_Tome_SummBoneGolem",
    spellId = spellIds.boneGolem
  },
  {
    id = "bk_Tome_SummCrysGolem",
    spellId = spellIds.crystalGolem
  },
  {
    id = "bk_Tome_SummFleshAtronach",
    spellId = spellIds.fleshAtronach
  },
  {
    id = "bk_Tome_SummIronAtronach",
    spellId = spellIds.ironGolem
  },
  {
    id = "bk_Tome_SummSwampAtronach",
    spellId = spellIds.swampMyconid
  },
  {
    id = "bk_Tome_SummTelvanniAtronach",
    spellId = spellIds.telvanniMyconid
  },
}

local function registerSpells()
  framework.spells.createBasicSpell({
    id = spellIds.ashGolem,
    name = "����� ���������� ������",
    effect = tes3.effect.summonAshGolem,
    range = tes3.effectRange.self,
    duration = 60
  })
  framework.spells.createBasicSpell({
    id = spellIds.boneGolem,
    name = "����� ��������� ������",
    effect = tes3.effect.summonBoneGolem,
    range = tes3.effectRange.self,
    duration = 60
  })
  framework.spells.createBasicSpell({
    id = spellIds.crystalGolem,
    name = "����� ���������������� ������",
    effect = tes3.effect.summonCrystalGolem,
    range = tes3.effectRange.self,
    duration = 60
  })
  framework.spells.createBasicSpell({
    id = spellIds.fleshAtronach,
    name = "����� �������� �����",
    effect = tes3.effect.summonFleshAtronach,
    range = tes3.effectRange.self,
    duration = 60
  })
  framework.spells.createBasicSpell({
    id = spellIds.ironGolem,
    name = "����� ��������� ������",
    effect = tes3.effect.summonIronGolem,
    range = tes3.effectRange.self,
    duration = 60
  })
  framework.spells.createBasicSpell({
    id = spellIds.swampMyconid,
    name = "����� ��������� ��������",
    effect = tes3.effect.summonSwampMyconid,
    range = tes3.effectRange.self,
    duration = 60
  })
  framework.spells.createBasicSpell({
    id = spellIds.telvanniMyconid,
    name = "����� �������� ��������",
    effect = tes3.effect.summonTelvanniMyconid,
    range = tes3.effectRange.self,
    duration = 60
  })
  
  framework.tomes.registerTomes(tomes)
end

event.register("MagickaExpanded:Register", registerSpells)