local configTable = require("sve.journal.config")
local config = configTable[1]

local function saveConfig()
    mwse.saveConfig("journal search and edit", config)
----mwse.log("mwse.saveConfig(\"journal search and edit\", config)")
end

local function setSliderLabelAsPercentage(self)
    local newValue = ""

    if self.elements.slider then
        newValue = self.elements.slider.widget.current + self.min
    end
  
    self.elements.label.text = self.label .. ": " .. newValue .. "%"

end

local function setSliderLabelAsTenthPercentage(self)
    local newValue = ""

    if self.elements.slider then
        newValue = tostring( tonumber( self.elements.slider.widget.current + self.min ) / 10 )
    end
  
    self.elements.label.text = self.label .. ": " .. newValue .. "%"

end

local function enabledGetText(self)
    local text = (
        self.variable.value and 
        "�������" or 
        "��������"
    )
    -- one frame timer to run after MCM page is constructed
    local state = self.variable.value
    timer.start({duration=0.01, type=timer.real, callback = function()
       local menuMCM = tes3ui.findMenu(tes3ui.registerID("MWSE:ModConfigMenu"))
       local contents = menuMCM:findChild(tes3ui.registerID("Label: Mod Config Options"))
       if contents ~= nil then
       	  contents.parent.parent.visible = state
       end
    end } )       
    return text
end

local function restoreGetText(self)
   return "Restore"
end

local function restoreDefaults()
--mwse.log("restore config = configTable[2] ...")
   for key, value in pairs(configTable[2]) do
      if type(value) == "table" then
         for key2,value2 in pairs(value) do
            config[key][key2]=value2
--mwse.log("config[%s][%s]=%s", tostring(key), tostring(key2), tostring(value2))
         end
      else
         config[key] = value
--mwse.log("config[%s]=%s", tostring(key), tostring(value))
      end
    end
    local menuMCM = tes3ui.findMenu(tes3ui.registerID("MWSE:ModConfigMenu"))
    local contents = menuMCM:findChild(tes3ui.registerID("PartDragMenu_main"))
    local children = contents:findChild(tes3ui.registerID("PartScrollPane_pane")).children
    for i = 1, #children do
       if children[i].text == "Journal Search And Edit" then
          children[i]:triggerEvent("mouseClick")
       end
    end
end

local function registerRestoreDefaultsButton()
   local menuMCM = tes3ui.findMenu(tes3ui.registerID("MWSE:ModConfigMenu"))
   local label = menuMCM:findChild(tes3ui.registerID("Label: Default Settings"))
   label.parent.parent.children[1]:register("mouseClick", restoreDefaults)
end    

local function padOptionsBottom(self)
    local menuMCM = tes3ui.findMenu(tes3ui.registerID("MWSE:ModConfigMenu"))
    local contents = menuMCM:findChild(tes3ui.registerID("Label: Mod Config Options"))
    if contents ~= nil then
       contents.parent.paddingBottom = 5
    end
end

return {
    name = "�������������� ��������",
    pages = {
        {
            label = "������������",
            class = "SideBarPage",
            components = {
               {
               class = "Category",
               label = "�������������� ��������",
               components = {
	           {
                   class = "YesNoButton",
                   description = "�������� ��� ��������� ���",
                   variable = {
                                id = "enabled",
                                class = "TableVariable",
                                table = config,
                     },
                   postCreate = enabledGetText,
                   getText = enabledGetText,
                   },
		   },
	       },
	       {
               class = "Category",
               label = "���� �������� ����",
	       postCreate = padOptionsBottom,
               components = {
               {
               class = "Category",
               label = "������� �������",
               components = {
	           {
                   class = "KeyBinder",
               	   label = "������� �������",
                   description = "���������� ������� ��� �������� ��������, ������ ������� \"J\" �� ���������.",
                   variable = {
                                id = "closeKeyInfo",
                                class = "TableVariable",
                                table = config,
                              },
                   },
	           {
                   class = "KeyBinder",
               	   label = "���������� ����� - ������",
                   description = "��������� ������� ��� ������ ���������� ���������� ����� �� ������.\n\n�������, ����� ������� � ���������� ����������.\n������, ����� ������� � ���������� ����������.",
                   variable = {
                                id = "nextMatchKeyInfo",
                                class = "TableVariable",
                                table = config,
                              },
                   },
	           {
                   class = "KeyBinder",
               	   label = "���������� ����� - �����",
                   description = "��������� ������� ��� ������ ���������� ���������� ����� �� ������.\n\n�������, ����� ������� � ���������� ����������.\n�������, ����� ������� � ���������� ����������.",
                   variable = {
                                id = "prevMatchKeyInfo",
                                class = "TableVariable",
                                table = config,
                              },
                   },
	           {
                   class = "KeyBinder",
               	   label = "���������� �����",
                   description = "��������� ������� ��� ����������� ������ ���������� ���������� � �������� ����������� ������, �� ��������� ����� ���� ����� ��� �������� �������; ������� ���������� ������ ���������� ��� ������� ������ ������/�����.\n\n�������, ����� ������� � ���������� ����������.\n�������, ����� ������� � ���������� ����������.",
                   variable = {
                                id = "contMatchKeyInfo",
                                class = "TableVariable",
                                table = config,
                              },
                   },
	           {
                   class = "KeyBinder",
               	   label = "������� ������ ��������������",
                   description = "��������� �������, ����� ������� ������ ��� ��������� ������ ������� ��� ��������������.\n\n����� �� ������ ��������������, ����������� �� ��������� ������� ������.",
                   variable = {
                                id = "selectEditDownKeyInfo",
                                class = "TableVariable",
                                table = config,
                              },
                   },
	           {
                   class = "KeyBinder",
               	   label = "������� ������� ��������������",
                   description = "��������� �������, ����� ������� ��������� ��� ���������� ������ ������� ��� ��������������.\n\n����� �� ������ ��������������, ����������� �� ������ ������� ������.",
                   variable = {
                                id = "selectEditUpKeyInfo",
                                class = "TableVariable",
                                table = config,
                              },
                   },
	           {
                   class = "KeyBinder",
               	   label = "�������� ��������",
                   description = "��������� ������� ��� ������� ����� ��������.",
                   variable = {
                                id = "newPageInsertKeyInfo",
                                class = "TableVariable",
                                table = config,
                              },
                   },
	           {
                   class = "KeyBinder",
               	   label = "������� ��������",
                   description = "��������� ������� ��� �������� ���� � ����� ��� ����� ��������.\n\n�������� ������ - ����������� ������� � ����� ������.",
                   variable = {
                                id = "deleteWordKeyInfo",
                                class = "TableVariable",
                                table = config,
                              },
                   },
	           {
                   class = "KeyBinder",
               	   label = "������� ������",
                   description = "���������� ������� ��� �������� ���� ������������� ��������� ������.",
                   variable = {
                                id = "deleteEntryKeyInfo",
                                class = "TableVariable",
                                table = config,
                              },
                   },
	           {
               	   label = "��������� � ����� �� ������ ��������������",
                   class = "KeyBinder",
                   description = "��������� ������� ��� ���������� ���������� ������ � ������ �� ������ ��������������.",
                   variable = {
                                id = "saveEditKeyInfo",
                                class = "TableVariable",
                                table = config,
                              },
                   },
	           {
                   class = "KeyBinder",
               	   label = "����� �� ������ ������ ��� ��������������",
                   description = "��������� ������� ��� ������ �� ������ ������ ��� �������������� ��� ����������.",
                   variable = {
                                id = "exitKeyInfo",
                                class = "TableVariable",
                                table = config,
                              },
                   },
	           {
                   class = "KeyBinder",
               	   label = "����������� �������� �����",
                   description = "��������� �������, ����� ����������� �������� ������. �������, ����� ������� � ��������� ��������; �������, ����� ���������� ������� �������� ������.",
                   variable = {
                                id = "nextPageKeyInfo",
                                class = "TableVariable",
                                table = config,
                              },
                   },
	           {
                   class = "KeyBinder",
               	   label = "����������� �������� �����",
                   description = "��������� �������, ����� ����������� �������� �����. �������, ����� ������� � ���������� ��������; �������, ����� ���������� ������� �������� �����.",
                   variable = {
                                id = "prevPageKeyInfo",
                                class = "TableVariable",
                                table = config,
                              },
                   },
	           {
                   class = "KeyBinder",
               	   label = "�������� ��������� �����������",
                   description = "��������� ������� ��� ������� ���������� �����������, ������������ �� ����, ������� �� ���������.\n\n�������� � ����������� ���������, ����� �������� � ����� ��������������, ���� ����������� �� ��������.",
                   variable = {
                                id = "nextImageKeyInfo",
                                class = "TableVariable",
                                table = config,
                              },
                   },
	           {
                   class = "KeyBinder",
               	   label = "�������� ���������� �����������",
                   description = "��������� ������� ��� ������� ����������� �����������, ������������ �� ����, ������� �� ���������.\n\n�������� � ����������� ���������, ����� �������� � ����� ��������������, ���� ����������� �� ��������.",
                   variable = {
                                id = "prevImageKeyInfo",
                                class = "TableVariable",
                                table = config,
                              },
                   },
                   {
		   label = "��� ��������������� �����������",
                   class = "Slider",
                   description = "������� ���������� ��� ���������� �������� �����������",
		   min = 1,
		   max = 25,
		   step = 1,
		   jump = 5,
                   variable = {
                                id = "incrImageScaleStep",
                                class = "TableVariable",
                                table = config,
                              },
                   postCreate = setSliderLabelAsPercentage,
                   updateValueLabel = setSliderLabelAsPercentage,
                   },
	           {
                   class = "KeyBinder",
               	   label = "��������� ������� �����������",
                   description = "��������� ������� ��� ���������� �������� �������� �����������.",
                   variable = {
                                id = "incrImageScaleKeyInfo",
                                class = "TableVariable",
                                table = config,
                              },
                   },
	           {
                   class = "KeyBinder",
               	   label = "��������� ������� �����������",
                   description = "��������� �������, ����� ��������� ������� ������� �����������.",
                   variable = {
                                id = "decrImageScaleKeyInfo",
                                class = "TableVariable",
                                table = config,
                              },
                   },
                   {
                   label = "���������� �������� ������ ��������� �����������",
                   class = "Slider",
                   description = "���������� ���������� ��� ���������� ������� �������� �����������.",
		   min = 1,
		   max = 25,
		   step = 1,
		   jump = 5,
                   variable = {
                                id = "incrImageFineScaleStep",
                                class = "TableVariable",
                                table = config,
                              },
                   postCreate = setSliderLabelAsTenthPercentage,
                   updateValueLabel = setSliderLabelAsTenthPercentage,
                   },
	           {
                   class = "KeyBinder",
               	   label = "��������� ������� �����������",
                   description = "��������� �������, ����� ��������� ������� ����������� � ��������� ��������.",
                   variable = {
                                id = "incrImageFineScaleKeyInfo",
                                class = "TableVariable",
                                table = config,
                              },
                   },
	           {
                   class = "KeyBinder",
               	   label = "��������� ������� �����������",
                   description = "��������� �������, ����� ��������� ������� ����������� � ��������� ��������.",
                   variable = {
                                id = "decrImageFineScaleKeyInfo",
                                class = "TableVariable",
                                table = config,
                              },
                   },
		 },
	       },
	       {
               class = "Category",
               label = "�������� ������������� � ������������",
               components = {
	           {
                   label = "������� �������� ���������� �� ������ �����",
                   class = "TextField", 
                   description = "���������, ������������ � ��� ������, ���� ��� ������� �������� ���������� �������������� ���������� �����.", 
                   sNewValue = "������� �������� ���������� �� ������ �����:%s",
                   variable = {
                                id = "messageNoBookArt",                                
                                class = "TableVariable",
                                table = config,
                                defaultSetting = "�� ����� ����� ��������, �� �� ������ ������������ �� � ��������.",
                              },
                   },
	           {
                   label = "�������� ��������� �� ����� �����",
                   class = "TextField", 
                   description = "���������, ������� ����������, ����� ����� �������� �� ����� ���� ����������� � �������� ��� ������� � ������.", 
                   sNewValue = "�������� ��������� �� ����� �����:%s",
                   variable = {
                                id = "messageNewBookArt",                                
                                class = "TableVariable",
                                table = config,
                                defaultSetting = "�� ����� ����� �������� ��� ������������� � ��������.",
                              },
                   },
	           {
                   label = "�������� ��������� �� ����� �����",
                   class = "TextField", 
                   description = "���������, ������� ���������� ��� ��������� ���������� ����� ������� �������� � ��������� ��� ������� � ������.", 
                   sNewValue = "�������� ��������� �� ����� �����:%s",
                   variable = {
                                id = "messageNewBookArts",                                
                                class = "TableVariable",
                                table = config,
                                defaultSetting = "�� ����� ����� �������� ��� ������������� � ��������.",
                              },
                   },
                 },
	       },
	       {
               class = "Category",
               label = "����������� � �����",
	       postCreate = padOptionsBottom,
               components = {
               {
               class = "Category",
               components = {
	           {
                   label = "����� ��������� ������ �������",
                   class = "TextField", 
                   description = "������(�), ������������(��) �����(�) ���������(�) ������(�).\n\n��� ���������� ���������, ��� ���� ������ ����� ���� ����� � ������ ��������������.\n\n������ ������ � �������.", 
                   sNewValue = "����� ��������� ������ �������:%s",
                   variable = {
                                id = "newTextLine",                                
                                class = "TableVariable",
                                table = config,
                                defaultSetting = ">",
                              },
                    },
                        {
                            label = "����� �������������� - ������� �����/���� - ������� ������� ��� �����������",
                            class = "Slider",
                            description = "� ������ �������������� ~����� �������� ������������ �����/������ � ������� ������ �� ��������� �����/����.\n���������� ��� �������� �� ������� ���������� ��������, ���������� - �� �������",
				min = 5,
				max = 100,
				step = 1,
				jump = 5,
                            variable = {
                                id = "cursorUpDownJumpChar",
                                class = "TableVariable",
                                table = config,
                            },
                        },
                        {
                            label = "������ ������ ��������� ����",
                            class = "YesNoButton",
                            description = "��������, ����� ������ 2-�, 3-�, .. ������ (��� �� ����) ��������� �� ������ ��������, ����� ���������� ������ ����� ��� ��������������.",
                            variable = {
                                id = "hideRedundantDateHeaders",
                                class = "TableVariable",
                                table = config,
                            },
                        },
                        {
                            label = "���������� ����� ����������",
                            class = "Slider",
                            description = "������� ������������ ������������ ����� �����������. ��������� ������������ ����� �����������, ������ ����� ��� ��������������.",
                            variable = {
                                id = "topicSpaceCompression",
                                class = "TableVariable",
                                table = config,
                            },
                            postCreate = setSliderLabelAsPercentage,
                            updateValueLabel = setSliderLabelAsPercentage,
                        },
                        {
                            label = "�������� �������������� ������� (������������)",
                            class = "Slider",
                            description = "�������� �������� ����� ���������� ��� ��������� ������� ������ ����/���� �������� ��� ����/������/�����.  �������� �������� ���������� ~100�� ��� ������������ ������������. ��������� �������� ��������������� �������, ���������, ���� ��������������� ������� ��������� ��������.",
				min = 100,
				max = 1000,
				step = 5,
				jump = 50,
                            variable = {
                                id = "pageTurnDelay",
                                class = "TableVariable",
                                table = config,
                            },
                        },
                        {
                            label = "�������� ���������� (������������)",
                            class = "Slider",
                            description = "��������, ������������ ��� ������� ���������� ����������������� ���������� ����� ������� ����� ��� ������� ������. �������� 20-100 ����������� ������� ��� ������������ ������������. ���������, ���� ���������� �������� ��������� ��������.",
				step = 5,
				jump = 10,
				min = 10,
				max = 200,
                            variable = {
                                id = "uiLatency",
                                class = "TableVariable",
                                table = config,
                            },
                        },
		    },
		 },
	       },
	       },
               {
               class = "Category",
               label = "������������ ���� �� ���������",
               components = {
	           {
                      class = "YesNoButton",
                      description = "����� �� ��������� ��������",
		      label = "Default Settings",
                      variable = {
                                   id = "restoreDefaults",
                                   class = "TableVariable",
                                   table = config,
                        },
                        getText = restoreGetText,
		        postCreate = registerRestoreDefaultsButton,
                     },
                  },
               },
            },
            },
	    },
            sidebarComponents = {
                {
                    class = "MouseOverInfo",
                    text = "MWSE �������������� �������� 1.0.\n\n��� ������:\n������ ������� ������� ����� � ������.\n������� ������� �������, ����� ����� ����/���� ���������� �� �������� ���������.\n��������� ������� ������� ����� ����� ���������� �� ����/���� ���������.\n\n��� ��������������, ��������� �� ������� ������� � ������� ������� ������ (�������� ������� �� ��������� �����/����) � �������� ���������.\n\n��������� � �������� � ������������ ������ �������� ������� ������."
                },
                {
                    class = "Category",
                    label = "��:",
                    components = {
                        {
                            class = "Hyperlink",
                            text = "Svengineer99",
                            exec = "start https://www.nexusmods.com/morrowind/users/1121630?tab=user+files",
                        },
                     },
                },
                {
                    class = "Category",
                    label = "��������� �������:",
                    components = {
                        {
                            class = "Hyperlink",
                            text = "Hrnchamd",
                            exec = "start https://www.nexusmods.com/morrowind/users/843673?tab=user+files",
                        },
                        {
                            class = "Hyperlink",
                            text = "NullCascade",
                            exec = "start https://www.nexusmods.com/morrowind/users/26153919?tab=user+files",
                        },
                        {
                            class = "Hyperlink",
                            text = "Petethegoat",
                            exec = "start https://www.nexusmods.com/morrowind/users/45692?tab=user+files",
                        },
                        {
                            class = "Hyperlink",
                            text = "Greatness7",
                            exec = "start https://www.nexusmods.com/morrowind/users/64030?tab=user+files",
                        },
                        {
                            class = "Hyperlink",
                            text = "Merlord",
                            exec = "start https://www.nexusmods.com/morrowind/users/3040468?tab=user+files",
                        },
                        {
                            class = "Hyperlink",
                            text = "Danae",
                            exec = "start https://www.nexusmods.com/morrowind/users/1233897?tab=user+files",
                        },
                    },
                },
            },
        },
    },
    onClose = saveConfig,
}

