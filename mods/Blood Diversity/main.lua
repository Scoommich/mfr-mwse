
local config = require("Blood Diversity.mcm").config

-- For vampirism we'll use the spell tick event to remove/add as needed.
local function onVampirismTick(e)
	if config.modEnabled and config.vampBlood then
		local object = e.target.object
		if (e.sourceInstance.state == tes3.spellState.ending) then
			object.blood = 0
		elseif (object.blood == 0) then
			object.blood = 1
		end
	end
end

local function onGhostTick(e)
	if config.modEnabled and config.ghostBlood then
		local object = e.target.object
		if (e.sourceInstance.state == tes3.spellState.ending) then
			object.blood = 0
		elseif (object.blood == 0) then
			object.blood = 4
		end
	end
end

local function onInitialized(e)
	-- Our data doesn't need to be global. It can be garbage collected
	local data = require("Blood Diversity.data")

	-- For creatures, we only need to do this once at the start.
	if config.modEnabled then
		for object in tes3.iterateObjects(tes3.objectType.creature) do
			local bloodOverride = data.bloodTable[object.mesh:lower()]
			if (bloodOverride) then
				object.blood = bloodOverride
				--mwse.log("[Blood Diversity] Setting blood to type %d for %s", bloodOverride, object)
			end
		end
	end

	-- Handle vampires.
	event.register("spellTick", onVampirismTick, { filter = tes3.getObject("vampire attributes") })
	-- Handle Ghost NPCs.
	event.register("spellTick", onGhostTick, { filter = tes3.getObject("ghost ability") })

	mwse.log("[Blood Diversity INFO] Initialized")
end
event.register("initialized", onInitialized)
