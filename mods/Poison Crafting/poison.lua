--[[
	Plugin: mwse_PoisonCrafting.esp
--]]

local g7a = require("Poison Crafting.common")

local this = {
	ignore = {},
	enable = true,
}

local player
local currentPoison
local alreadyPoisonList

-- ENEMY

this.classPoisonChance = {
	["alchemist"]          = 30,
	["alchemist service"]  = 30,
	["apothecary"]         = 30,
	["apothecary service"] = 30,
	["assassin"]           = 30,
	["assassin service"]   = 30,
	["bard"]               = 30,
	["scout"]              = 30,
	["nightblade"]         = 30,
	["nightblade service"] = 30,
	["rogue"]              = 30,
	["witch"]              = 30,
}


-- USAGE

local function readyPoison()
	-- retrieve previous poison
	if g7a.config.usePoisonRecovery and g7a.data.poison then
		mwscript.addItem{reference=player, item=g7a.data.poison}
	end

	-- store the current poison
	g7a.data.poison = currentPoison.id

	-- remove it from inventory
	mwscript.removeItem{reference=player, item=currentPoison.id}

	-- show poison readied text
	tes3.messageBox{message=currentPoison.name .. " ������ ��� ��������� �����"}

	-- display poison ready hud
	g7a.enableHUD()
end


local function applyPoison(poison, target)
	-- inflict poison on target
	mwscript.equip{reference=target, item=poison}

	-- show poison applied text
	if g7a.config.msgPoisonApplied then
		tes3.messageBox{message="���� ���� ���� ���������!"}
	end
end


local function enemyPoison(source, target)
	-- poisoning chance based on npc class

	if alreadyPoisonList[source.id] then
		-- already used
		return
	end

	local class = source.object.class
	if not class then
		-- is a creature
		return
	end

	local chance = this.classPoisonChance[class.id:lower()]
	if not chance then
		-- invalid class

		return
	end

	if chance >= math.random(100) then
		local list = tes3.getObject("leveled_poison_enemy")
		if not list then
			mwse.log('[Potion Crafting] tes3.getObject("leveled_poison_enemy") = nil error. Target.id = '..target.id or 'nil ')
			return
		end
		-- bypass events
		this.enable = false
		g7a.equip{reference=target, item=list:pickFrom().id}
		this.enable = true
	end

	alreadyPoisonList[source.id] = true
end

-- PROJS

local function appendProjectile(id, poison)
	local list = g7a.data.projectiles[id] or {}

	-- save poison projectile
	table.insert(list, poison)

	-- ensure list is updated
	g7a.data.projectiles[id] = list
end


local function removeProjectile(id)
	local list = g7a.data.projectiles[id] or {}

	-- uses oldest projectile
	return table.remove(list, 1)
end

function this.onAttack(e)

	if not e.mobile.readiedWeapon then return	end

	local source = e.reference
	local target = e.targetReference
	local action = e.mobile.actionData
	local weapon = e.mobile.readiedWeapon
	local poison = g7a.data.poison

	-- handle player poison attacks
	if (source == player) and poison then

		if weapon.object.type > 8 then
			appendProjectile(weapon.object.id, poison)
		elseif target and action.physicalDamage > 0 then
			applyPoison(poison, target)
		else -- ignore misses
			return
		end
		g7a.data.poison = nil
		g7a.disableHUD()
		return
	end

	-- handle others poison attacks
	if (source ~= player) and target then

		if action.physicalDamage == 0 then
			-- ignore misses
		elseif weapon.object.type > 8 then
			-- ignore ranged
		else
			enemyPoison(source, target)
		end

		return
	end
end

--

function this.onProjectileHitActor(e)
	--
	local source = e.firingReference
	local weapon = e.firingWeapon
	local target = e.target

	if not (weapon and target) then
		return
	elseif (source == target) then
		return
	end

	if (source ~= player) then
		enemyPoison(source, target)
		return
	end

	local poison = removeProjectile(weapon.id)
	if poison then
		applyPoison(poison, target)
		return
	end
end


function this.onProjectileExpire(e)
	--
	local source = e.firingReference
	local weapon = e.firingWeapon

	if (source == player) and weapon then
		removeProjectile(weapon.id)
	end
end

--

local function isHotKeyDown(hk)
	local IC = tes3.worldController.inputController
	if IC:isKeyDown(hk.keyCode) then
		if hk.isAltDown then if IC:isAltDown() then return true end
		elseif hk.isControlDown then if IC:isControlDown() then return true end
		elseif hk.isShiftDown then if IC:isShiftDown() then return true end
		else return true
		end
	end
	return false
end

function this.onEquip(e)
	if e.reference ~= player then return end
	local w = player.mobile.readiedWeapon
	if not w then return end

	if this.enable == true
	and e.item.objectType == tes3.objectType.alchemy
	and w.object.objectType == tes3.objectType.weapon
  and isHotKeyDown(g7a.config.poisonHotkey) --mwse.virtualKeyPressed(g7a.config.poisonHotkey)
	and this.ignore[e.item.id] == nil
	then
		if not w then
			tes3.messageBox{message="� ��� ������ ���� ������, �� ������� ����� ������� ��"}
			return
		end

		currentPoison = e.item
		if g7a.config.msgReadyPoison then
			tes3.messageBox{
				message = '��������� "' .. currentPoison.name .. '" � ����� ��������� �����?',
				buttons = {"��", "���"},
				callback = function(e)
						-- Apply poison to your next attack? -- 0. Yes -- 1. No
						if e.button == 0 then	timer.frame.delayOneFrame(readyPoison) end
				end
			}
		else
			timer.frame.delayOneFrame(readyPoison) -- do not call on same frame as equip!
		end
		return false
	end
end


function this.onUnequipped(e)
	--
	if e.reference == player
	and g7a.data.poison
	and not g7a.config.usePoisonRecovery
	and e.item.objectType == tes3.objectType.weapon
	and not player.mobile.readiedWeapon -- allow weapon swaps
	then
			timer.frame.delayOneFrame( function()
				mwscript.playSound{reference=player, sound="Potion Fail"}
			end)
			g7a.data.poison = nil
			g7a.disableHUD()
	end
end

--
function this.onCombatStarted(e)
	-- NPCs are limited to poisoning once per combat session.
	-- When new combat sessions are started reset that limit.
	alreadyPoisonList[e.actor.reference.id] = nil
end

function this.onLoaded(e)
	-- update outer scoped vars
	player = tes3.getPlayerRef()

	-- clear npc posioners list
	alreadyPoisonList = {}

	if player.data.g7a.poison then
		g7a.enableHUD()
	else
		g7a.disableHUD()
	end
end

function this.register()
	event.register("loaded", this.onLoaded)
	event.register("attack", this.onAttack)
	event.register("projectileHitActor", this.onProjectileHitActor)
	event.register("projectileExpire", this.onProjectileExpire)
	event.register("equip", this.onEquip)
	event.register("unequipped", this.onUnequipped)
	event.register("combatStarted", this.onCombatStarted)
end

function this.unregister()
	event.unregister("loaded", this.onLoaded)
	event.unregister("attack", this.onAttack)
	event.unregister("projectileHitActor", this.onProjectileHitActor)
	event.unregister("projectileExpire", this.onProjectileExpire)
	event.unregister("equip", this.onEquip)
	event.unregister("unequipped", this.onUnequipped)
	event.unregister("combatStarted", this.onCombatStarted)
end

return this
