--[[
	Plugin: mwse_PoisonCrafting.esp
--]]

local this = {
	config = require("Poison Crafting.mcm").config
}

function this.saveConfig()
	this.loadLabels() -- ensures labels are reloaded after changes
	json.savefile("config/MWSE Poison Crafting", this.config, {indent=true})
end

local menuMultiId = tes3ui.registerID("MenuMulti")
local weaponLayoutId = tes3ui.registerID("MenuMulti_weapon_layout")
local poisonBlockId = tes3ui.registerID("poison_craft_weapon_block")

function this.prepareHUD(e)
	if not e.newlyCreated then return	end

  local multiMenu = e.element or tes3ui.findMenu(menuMultiId)
  local weaponLayout = multiMenu:findChild(weaponLayoutId)
  local poisonBlock = weaponLayout:createBlock{ id = poisonBlockId }
  poisonBlock.autoWidth = true
  poisonBlock.autoHeight = true
  poisonBlock.visible = false

  local poisonFillBar = poisonBlock:createFillBar{ id = poisonFillBarId }
  poisonFillBar.widget.fillColor = {0.4, 0.8, 0.3}
  poisonFillBar.widget.showText = false
  poisonFillBar.width = 36
  poisonFillBar.height = 6
  poisonFillBar.widget.max = 100
  poisonFillBar.widget.current = 100
  poisonFillBar.alpha = tes3.worldController.menuAlpha
	multiMenu:updateLayout()
end

function this.enableHUD()
	local multiMenu = tes3ui.findMenu(menuMultiId)
  if multiMenu then
  	local weaponLayout = multiMenu:findChild(weaponLayoutId)
  	local poisonBlock = weaponLayout:findChild(poisonBlockId)
    if poisonBlock then
      poisonBlock.visible = true
    else
     	mwse.log('[Potion Crafting] error: no poisonBlock = multiMenu:findChild(poisonBlockId) ')
    end
  end
end

function this.disableHUD()
	local multiMenu = tes3ui.findMenu(menuMultiId)
  if multiMenu then
  	local weaponLayout = multiMenu:findChild(weaponLayoutId)
  	local poisonBlock = weaponLayout:findChild(poisonBlockId)
    if poisonBlock then
      poisonBlock.visible = false
    end
  end
end

-- Labels

do -- applyLabel
	local assets = {icon=".tga", model=".nif"}
	local qualities = {"exclusive", "quality", "fresh", "standard", "cheap", "bargain"}

	function this.applyLabel(potion)
		for asset, suffix in pairs(assets) do
			local current = potion[asset]:lower()
			for _, quality in pairs(qualities) do
				if current:find(quality) then
					local effect = potion.effects[1].id
					potion[asset] = "MFR\\m\\" .. quality .. "_" .. effect .. suffix
					break
				end
			end
		end
	end
end

do -- clearLabel
	local assets = {
		icon = {
			bargain = "m\\tx_potion_bargain_01.dds",
			cheap = "m\\tx_potion_cheap_01.dds",
			exclusive = "m\\tx_potion_exclusive_01.dds",
			fresh = "m\\tx_potion_fresh_01.dds",
			quality = "m\\tx_potion_quality_01.dds",
			standard = "m\\tx_potion_standard_01.dds",
		},
		model = {
			bargain = "m\\misc_potion_bargain_01.nif",
			cheap = "m\\misc_potion_cheap_01.nif",
			exclusive = "m\\misc_potion_exclusive_01.nif",
			fresh = "m\\misc_potion_fresh_01.nif",
			quality = "m\\misc_potion_quality_01.nif",
			standard = "m\\misc_potion_standard_01.nif",
		},
	}
	function this.clearLabel(potion)
		for asset, qualities in pairs(assets) do
			local current = potion[asset]:lower()
			for quality, filename in pairs(qualities) do
				if current:find(quality) then
					potion[asset] = filename
					break
				end
			end
		end
	end
end


function this.loadLabels()
	local useLabels = this.config.useLabels

	for potion in tes3.iterateObjects(tes3.objectType.alchemy) do
		local isLabelled = (
			potion.model:lower():find("^MFR\\m\\")
			and potion.icon:lower():find("^MFR\\m\\")
		)
		if not useLabels and isLabelled then
			this.clearLabel(potion)
		elseif useLabels and not isLabelled then
			this.applyLabel(potion)
		end
	end
end

-- Compatibility

function this.confirmMCP()
	-- Confirm that some required MCP features are enabled.

	local labelFix = tes3.hasCodePatchFeature(145)
	local usageFix = tes3.hasCodePatchFeature(166)
	if (labelFix and usageFix) then
		return
	end

	local function warning()
		tes3.messageBox{
			message = (
				"MWSE_PoisonCrafting.esp\n\n"
				.."The required MCP features for this mod are not enabled!\n\n"
				.."Please enable the following:\n\n"
				.."Game mechanics -> Quality-based potion icons/models\n\n"
				.."Mod specific -> Scriptable potion use"
			),
			buttons = {"Ok"},
		}
	end

	event.register("menuEnter", warning, {doOnce = true})
end

function this.equip(args)
	-- compatibility for controlled consumption
	-- prevents enemy poisons causing cooldowns
	local cc = include("nc.consume.interop")

	if not cc then
		-- controlled consumption not installed
		this.equip = mwscript.equip
	else
		-- bypass controlled consumption checks
		function this.equip(args)
			local bool = cc.skipNextConsumptionCheck
			cc.skipNextConsumptionCheck = true
			mwscript.equip(args)
			cc.skipNextConsumptionCheck = bool
		end
		print("[Poison Crafting] Controlled Consumption Detected")
	end

	this.equip(args)
end

function this.onLoaded(e)
	-- ensure data table exists
	local data = tes3.getPlayerRef().data
	data.g7a = data.g7a or {}

	-- also do projectile table
	data.g7a.projectiles = data.g7a.projectiles or {}

	-- create a public shortcut
	this.data = data.g7a
end

function this.register()
	event.register("loaded", this.onLoaded)
	event.register("uiActivated", this.prepareHUD, { filter = "MenuMulti" })
end

function this.unregister()
	event.unregister("loaded", this.onLoaded)
	event.unregister("uiActivated", this.prepareHUD, { filter = "MenuMulti" })
end

return this
