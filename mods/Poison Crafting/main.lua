--[[
	Poison Crafting v2.1 by Graitness7
	Upgrade code & rus lang by MintMike fullrest.ru
--]]
local common = require("Poison Crafting.common")
local apparatus = require("Poison Crafting.apparatus")
local poison = require("Poison Crafting.poison")
local modConfig = require("Poison Crafting.mcm")

function modConfig.onMod()
		common.register()
		apparatus.register()
		poison.register()
end

function modConfig.offMod()
		common.unregister()
		apparatus.unregister()
		poison.unregister()
end

local function init(e)

	if tes3.isModActive("Morrowind.esm") then
		-- load item labels
		common.loadLabels()
		-- check MCP status
		common.confirmMCP()

		if modConfig.config.modEnable then modConfig.onMod() end

		mwse.log("[Poison Crafting] Initialized v" .. tostring(common.config.version))
	end
end

local oldVersion = include("g7/a/common")
if oldVersion then
	mwse.log("[Poison Crafting] Old version detected. If you wish to play [Poison Crafting] ".. tostring(common.config.version) .. ", you must delete Data Files/MWSE/lua/g7/a folder files.")
else
	event.register("modConfigReady", modConfig.registerModConfig)
	event.register("initialized", init)
end