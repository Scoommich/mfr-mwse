
local this = {}

local prevConfig = mwse.loadConfig("Poison Crafting")
local myVersion = 1.0

if prevConfig and prevConfig.version and prevConfig.version == myVersion
then this.config = prevConfig
else this.config = {
		modEnable = true,
		msgPoisonApplied = true,
		msgReadyPoison = true,
		poisonHotkey = {
				keyCode = tes3.scanCode.lAlt,
				isShiftDown = false,
				isAltDown = false,
				isControlDown = false,
		},
		useBaseStats = true,
		useBonusProgress = true,
		useLabels = true,
		usePoisonRecovery = false,
		version = myVersion,
	}
end

--function this.onMod() end  -- defined in main.lua
--function this.offMod() end -- defined in main.lua

function this.registerModConfig()

	local template = mwse.mcm.createTemplate{ name = "����� � ���" }
	template:saveOnClose("Poison Crafting", this.config)

	local preferences = template:createSideBarPage{
		label = "���������",
		description = "������������ ��� �����, � ��� ����� ����������� ������� ������ ��� ��������� �������. ������������� �������� � ���� ����� �������� HUD � ��������� ������ ��������� ������� ������� � �����. ������ �� ������ ������ ���, ����������� � ������ ������. �������� �� �� ������� ������� 1-9, �� ������ ��� ���������, ����� ������� ������� (Alt).",
	}

	local block = preferences:createCategory{
		label = "MWSE Potion Crafting - ����� � ���\n������ " .. tostring(this.config.myVersion) .. " �� Greatness7, ��������� 0.1 �� MintMike",
	}

	block:createOnOffButton({
		label = "��������� ����",
		description = "�������� / ��������� �������� ���� � ����",
		variable = mwse.mcm:createTableVariable({ id = "modEnable", table = this.config }),
		callback = function()
			if this.config.modEnable then this.onMod() else this.offMod() end 
		end,
		buttonText = "***",
		restartRequired = true,
	})

	block:createYesNoButton({
		label = "����� ���������� ������� �� ������",
		description = "���� �������� �� ������ �� ������ ����� � ������ MFR. ������ ��������� � ������ � ��������� ������� �����, ������������ ��� ���������� �������� (����������� ���������� ����)",
		variable = mwse.mcm:createTableVariable({ id = "useLabels", table = this.config }),
	})

	block:createYesNoButton({
		label = "���������� ��� ��� ������ ������",
		description = "�� �����������, ���� �� ������� ��� ����� ������. ��� ��������� ������ ��� �� ����������� ������, ���������� �� ����� ����� ������������ � ���������.",
		variable = mwse.mcm:createTableVariable({ id = "usePoisonRecovery", table = this.config }),
	})

	block:createYesNoButton({
		label = "����������� ����� ������ �������",
		description = "���� ������ ������� ������� �� ������� ���������� ���������, � �� ��������� ������ ��������",
		variable = mwse.mcm:createTableVariable({ id = "useBaseStats", table = this.config }),
	})

	block:createYesNoButton({
		label = "�������������� ���� � �������",
		description = "�������������� ���� � ������� � ����������� �� ���������� �������� � ���������� ������",
		variable = mwse.mcm:createTableVariable({ id = "useBonusProgress", table = this.config }),
	})

	block:createKeyBinder{
		label = "������� ������� ��� ��������� ���",
		description = "������� ������� ������� �, ����� � ����� ������, ����������� ����� ��� �� (�� ���� ����)",
		inGameOnly = false,
		allowCombinations = true,
		variable = mwse.mcm:createTableVariable{
			inGameOnly = false,
			id = "poisonHotkey",
			table = this.config,
			defaultSetting = {
				keyCode = tes3.scanCode.lAlt,
				isShiftDown = false,
				isAltDown = true,
				isControlDown = false,
			}
		},
	}

	block:createYesNoButton({
		label = "������������� ��������� ���",
		description = "������������� �� �� ������ ������� ������ ����� �� ������ � �������� ��� (��/���)",
		variable = mwse.mcm:createTableVariable({ id = "msgReadyPoison", table = this.config }),
	})

	block:createYesNoButton({
		label = "C�������� �� ���������� �����",
		description = "��������� ��������� �������� ��� ������ ���������� �����",
		variable = mwse.mcm:createTableVariable({ id = "msgPoisonApplied", table = this.config }),
	})

	template:register()
end

return this