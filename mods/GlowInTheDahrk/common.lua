local common = {}

common.log = require("logging.logger").new({
	name = "Glow in the Dahrk",
})

--[[ CHECK rus encoding & load translations ]]

local rus = require("GlowInTheDahrk.i18n.rus") -- should be encoding UTF-8 to work with i18n
local rus1251 = require("GlowInTheDahrk.rus1251")
if not rus1251 then rus1251 = require("GlowInTheDahrk.i18n.eng") end

if not rus 
or not rus["mcm.modname"] 
or #rus["mcm.modname"] == #rus1251["mcm.modname"] -- i18n.rus.lua encoding == rus1251.lua encoding
then
	mwse.log("[GlowInTheDahrk] Loading rus1251 translations...")
	common.i18n = function(key) return rus1251[key] or key or "<no translate>" end
	--tr = setmetatable({}, {__call = function(self, key) return rus1251[key] or key or "<no translate>" end})
else
	common.i18n = mwse.loadTranslations("GlowInTheDahrk")
end

return common
