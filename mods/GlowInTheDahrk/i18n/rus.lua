return {
	["logConvertingLegacyMesh"] = "Converting legacy mesh: %s",
	["logLoadErrorDebugMeshData"] = "Invalid compiled mesh data: %s",
	["logLoadErrorDebugTextureMaterialMap"] = "Texture material map keys: %s",
	["logLoadErrorHeader"] = "Encountered the following issues when parsing mesh '%s':",
	["logMalformedAssetSwitchNodeIsNotCorrectType"] = "Encountered malformed mesh '%s'. NightDaySwitch is of type '%s' and not 'NiSwichNode'.",
	["logMeshParsed"] = "Mesh successfully parsed: %s",
	["mcm.addInteriorLights.description"] = " При включении, свет будет добавлен окнам в интерьерах, когда снаружи день.\n\nПо умолчанию: Вкл",
	["mcm.addInteriorLights.label"] = " Свечение окон в интерьерах",
	["mcm.addInteriorSunrays.description"] = " Эффект падающих в дневное время лучей от окон в интерьерах.\n\nПо умолчанию: Вкл",
	["mcm.addInteriorSunrays.label"] = " Солнечные лучи от окон в интерьерах",
	["mcm.info"] = "Glow in the Dahrk (Свет во Тьме)\n\nМодели и текстуры от Melchior Dahrk\nСкрипты от NullCascade.\n\nНаведите мышь на элемент для того, чтобы увидеть его описание.",
	["mcm.logLevel.DEBUG"] = "DEBUG",
	["mcm.logLevel.description"] = " Уровень логов mwse.log. Для обычной игры оставляйте уровень INFO.",
	["mcm.logLevel.ERROR"] = "ERROR",
	["mcm.logLevel.INFO"] = "INFO",
	["mcm.logLevel.label"] = "Log Level",
	["mcm.logLevel.NONE"] = "NONE",
	["mcm.logLevel.TRACE"] = "TRACE",
	["mcm.useVariance.description"] = " Окна зажигаются и гаснут постепенно в заданных границах времени.\n\nПо умолчанию: Вкл",
	["mcm.useVariance.label"] = " Окна снаружи зажигаются по одному",
	["mcm.varianceInMinutes.description"] = " Интервал времени до рассвета / после заката, в течение которого окна снаружи могут зажечься / погаснуть. Значение 30 означает, что окна могут на 30 минут погаснуть раньше рассвета / зажечься позже заката.\n\nПо умолчанию: 30",
	["mcm.varianceInMinutes.label"] = " Максимальный интервал (в минутах)",
	["mcm.modname"] = "Свет во Тьме",
}