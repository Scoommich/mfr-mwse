--[[
	Double Backstab Damage
	This mod allows for backstabs when using short blades.
]]--
--local mcm = require("mer.backstab.mcm")

local config = json.loadfile("config/mer_backstab_config")
if (not config) then
	config = {
  modEnable = true,
		showBackStabMsg = true,
		shortBladeOnly = true,
		stabsOnly = true
	}
end
local multiMin = 1.5
local multiMax = 3.0
local backstabDegrees = 80
local backstabAngle = (2 * math.pi) * (backstabDegrees / 360)

local debugMode = false
local function debugMessage(message)
	if debugMode then
		tes3.messageBox(message)
	end
end

local targetList = targetList or {}

local function onCombatStopped(e)
	targetList = {}
end

local function onDamage(e)
	for i=1, #targetList do
		if targetList[i]["attackerRef"] then
			local targetRef = e.reference
			local attackerRef = targetList[i]["attackerRef"]
			if targetRef == targetList[i]["targetRef"] then
				-- Check that target is facing away from attacker orientation is between -Pi and Pi. 
				-- We get the difference between attacker and target angles, and if it's greater than pie, 
				-- we've got the obtuse angle, so subtract 2*pi to get the acute angle.
				local attackerAngle = attackerRef.orientation.z
				local targetAngle = targetRef.orientation.z
				local diff = math.abs (attackerAngle - targetAngle )
				if diff > math.pi then
					diff = math.abs ( diff - ( 2 * math.pi ) )
				end
				-- If the player and attacker have the same orientation, then the attacker must be behind the target
				table.remove(targetList, i)
				if ( diff < backstabAngle ) then
					tes3.playSound({ reference = e.reference, sound = "critical damage" })
					if attackerRef == tes3.getPlayerRef() and config.showBackStabMsg then 
						tes3.messageBox("���� � �����!")
					end
				end
			end
		end
	end
end



local function onAttack(e)
	local attackerMobile = e.mobile
	local attackerRef = e.reference
	local targetMobile = e.targetMobile
	local targetRef = e.targetReference
	
	--Exit conditions
	local weapon = attackerMobile.readiedWeapon
	if not weapon then 
		debugMessage("No weapon")
		return 
	end
	if weapon.object.type ~= tes3.weaponType.shortBladeOneHand and config.shortBladeOnly then
		debugMessage("Not a short blade")
		return 	
	end
	if not targetRef then
		debugMessage("No target")
		return
	end
	if attackerMobile.actionData.attackDirection ~= 3 and weapon.object.type < 9 and config.stabsOnly then
		debugMessage("Not a stab")
		return
	end
	--/exit conditions

	-- Check that target is facing away from attacker orientation is between -Pi and Pi. 
	-- We get the difference between attacker and target angles, and if it's greater than pie, 
	-- we've got the obtuse angle, so subtract 2*pi to get the acute angle.
	local attackerAngle = attackerRef.orientation.z
	local targetAngle = targetRef.orientation.z
	local diff = math.abs (attackerAngle - targetAngle )
	if diff > math.pi then
		diff = math.abs ( diff - ( 2 * math.pi ) )
	end
	-- If the player and attacker have the same orientation, then the attacker must be behind the target
	if ( diff < backstabAngle and attackerMobile.actionData.physicalDamage > 0 ) then
		--get the damage multi from attacker stats
		local sneak = attackerMobile.sneak.current < 100 and attackerMobile.sneak.current or 100
		local agility = attackerMobile.agility.current < 100 and attackerMobile.agility.current or 100
		local damageMultiplier = multiMin + ( ( multiMax - multiMin ) * ( sneak / 100 ) * ( agility / 100 ) )
		attackerMobile.actionData.physicalDamage = attackerMobile.actionData.physicalDamage * damageMultiplier

		table.insert(targetList, {["targetRef"]=targetRef, ["attackerRef"]=attackerRef})
	end
end

local function getYesNoText (b)
		return b and tes3.findGMST(tes3.gmst.sYes).value or tes3.findGMST(tes3.gmst.sNo).value
end

local function getOnOffText (b)
		return b and "�������" or "��������"
end

local function onMod()
event.register( "attack", onAttack )
event.register( "damage", onDamage )
event.register( "combatStopped", onCombatStopped )
end
if config.modEnable then onMod() end

local function offMod()
event.unregister( "attack", onAttack )
event.unregister( "damage", onDamage )
event.unregister( "combatStopped", onCombatStopped )
end

local function toggleMod(e)
 config.modEnable = not config.modEnable
 e.source.text = getOnOffText(config.modEnable)
 if config.modEnable then onMod() else offMod() end  
end


--[[ MOD CONFIG MENU ]]--

local modConfig = {}
function modConfig.onCreate(container)
	
	local descriptionLabel = {}--global scope so we can update the description in click events

	local function toggleBackstabMsg(e)
		config.showBackStabMsg = not config.showBackStabMsg
		local button = e.source
		button.text = getYesNoText(config.showBackStabMsg)
		descriptionLabel.text = config.showBackStabMsg and 
			'��������� �� ����� � ����� � ���� ������������ �����.'
			or
			"������ ���� ������������ �����."
	end

	local function toggleAllWeapons(e)
		config.shortBladeOnly = not config.shortBladeOnly
		local button = e.source
		button.text = config.shortBladeOnly and tes3.findGMST(tes3.gmst.sYes).value or tes3.findGMST(tes3.gmst.sNo).value
		descriptionLabel.text = config.shortBladeOnly and 
			"�������� ����� � ����� ����� ���� �������� ������ ��� ������������� �������� �������." 
			or
			"�������� ����� � ����� ����� �������� � ����� ������� � �����." 
	end
	
	local function togglestabsOnly(e)
		config.stabsOnly = not config.stabsOnly
		local button = e.source
		button.text = config.stabsOnly and tes3.findGMST(tes3.gmst.sYes).value or tes3.findGMST(tes3.gmst.sNo).value
		descriptionLabel.text = config.stabsOnly and 
			"������ �������� ����� � ����� �������� ���������� �����." 
			or
			"����� �� ���������� � ����� ����� ���� �������� ��������� �����������." 
	end


	do
		local optionBlock = container:createThinBorder({})
		optionBlock.layoutWidthFraction = 1.0
		optionBlock.flowDirection = "top_to_bottom"
		optionBlock.autoHeight = true
		optionBlock.paddingAllSides = 10

	 local header = optionBlock:createLabel{ text = "����� � �����\n������ 1.3 �� Merlord" }
	 header.color = tes3ui.getPalette("header_color")
	 header.borderBottom = 25

	 local txtBlock = optionBlock:createBlock()
	 txtBlock.widthProportional = 1.0
	 txtBlock.autoHeight = true
	 txtBlock.borderBottom = 25

	 local txt = txtBlock:createLabel{}
	 txt.wrapText = true
	 txt.text = "��� �� ���������� �����, ����� �� �������� ������� ����."

	
		local function makeButton(parentBlock, labelText, buttonText, callBack)
			local buttonBlock	= parentBlock:createBlock({})
			buttonBlock.flowDirection = "left_to_right"
			buttonBlock.layoutWidthFraction = 1.0
			buttonBlock.autoHeight = true
			
			local label = buttonBlock:createLabel({ text = labelText })
			label.layoutOriginFractionX = 0.0

			local button = buttonBlock:createButton({ text = buttonText })
			button.layoutOriginFractionX = 1.0
			button.paddingTop = 3
			button:register("mouseClick", callBack)		
		end

		local buttonText = getOnOffText(config.modEnable)
		makeButton(optionBlock, '��������� ����', buttonText, toggleMod)
  buttonText = getYesNoText(config.showBackStabMsg)
		makeButton(optionBlock, '���������� ��������� �� ����� � �����?', buttonText, toggleBackstabMsg)
		buttonText = getYesNoText(config.shortBladeOnly)
		makeButton(optionBlock, '������ � ��������� ��������?', buttonText, toggleAllWeapons)
		buttonText = getYesNoText(config.stabsOnly)
		makeButton(optionBlock, '����� ������ � �����, � �� � ����� ����� ����, ����������� ��������� ����?', buttonText, togglestabsOnly)

				
		--Description pane
		local descriptionBlock = container:createThinBorder({})
		descriptionBlock.layoutWidthFraction = 1.0
		descriptionBlock.paddingAllSides = 10
		descriptionBlock.layoutHeightFraction = 1.0
		descriptionBlock.flowDirection = "top_to_bottom"
		
		--Do description first so it can be updated by buttons
		descriptionLabel = descriptionBlock:createLabel({ text = 
			"�������� ����� ����� � ����� ����������� ��������� ����. " .. 
			"��������� ����� ����� ���������� �� 1.5 �� 3-�������� ���������� �����������, ����������� �� �������� � ���������� ������. " ..
			"����� � ����� ����� ��������� ��� NPC, ��� � �����."
		})
		descriptionLabel.layoutWidthFraction = 1.0
		descriptionLabel.wrapText = true
		
	end
end

function modConfig.onClose(container)
	mwse.log("[merlord-backstab] Saving mod configuration:")
	mwse.log(json.encode(config, { indent = true }))
	json.savefile("config/mer_backstab_config", config, { indent = true })
end

-- When the mod config menu is ready to start accepting registrations, register this mod.
local function registerModConfig()
	 mwse.registerModConfig("����� � �����", modConfig)
end

event.register("modConfigReady", registerModConfig)
